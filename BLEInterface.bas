﻿Type=Service
Version=5.2
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Service Attributes 
	#StartAtBoot: False
'	#StartCommandReturnValue: android.app.Service.START_STICKY	

	'THIS SERVICE MODULE USES BleExtEx LIBRARY
	'THIS SERVICE MODULE IS NOT CURRENTLY IN USE IN THIS PROJECT

#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	
	Dim BT As BluetoothAdmin
	Dim Manager As BleManager
	Dim GATTServicesMap As Map
	
	Dim workerService As BleService
	Dim workerCharacteristic As BleCharacteristic
	
	Dim BatteryService As BleService
	Dim BatteryLevelCharacteristic As BleCharacteristic
	
	Dim NRFUartService As BleService
	Dim NRFUartReadCharacteristic As BleCharacteristic
	Dim NRFUartWriteCharacteristic As BleCharacteristic
	
	Dim DFUService As BleService
	
	Dim BatteryLevelReader As Timer
	
	Dim ConnectState As Int
	Dim cstateIdle As Int = 0
'	Dim cstateOnConnect As Int = 1
	Dim cstateConnect As Int = 2
'	Dim cstateOnDisconnect As Int = 3
'	Dim cstateOnClose As Int = 4
'	Dim cstateOnFinish As Int = 5

	Dim ByteConverter As ByteConverter

	Type NameAndMAC (Name As String, MAC As String)
	Dim ConnectedDevice As NameAndMAC
	Dim FoundDevices As List
	Dim MACList As List
	Dim ConnectToLastFlag As Boolean = True
End Sub

Sub Service_Create
	Try
		FoundDevices.Initialize	
		ConnectedDevice.Initialize
		GATTServicesMap.Initialize
		BT.Initialize("btEvent")
		Manager.Initialize("bleEvent")
		BatteryLevelReader.Initialize("ReadBatteryLevel", 3*60000) 'read n times in a minute
		ConnectState = cstateIdle
	Catch
		Log("Service_Create Error: " & LastException)
		StopService("") 'stop current
		ConnectState = cstateIdle
	End Try
	ConnectState = cstateIdle
End Sub

Sub Service_Start (StartingIntent As Intent)
End Sub

Sub Service_Destroy
	CallSub(Starter, "CloseTextwriter")
	StopService("") 'stop self
End Sub

Sub bleEvent_DeviceFound (Name As String, MacAddress As String)
	Log("DEVICE FOUND: " & Name & " (" & MacAddress & ")")
	If MacAddress <> Null Then
		If Name <> Null Then	
			If Name.Contains("BTL") Then
				Dim nameAndMac As NameAndMAC
				nameAndMac.Initialize
				nameAndMac.Name = Name
				nameAndMac.Mac = MacAddress
				If (ConnectToLastFlag) Then
					If MacAddress = Starter.lastConnectedMAC Then Manager.Connect(Starter.lastConnectedMAC, True)
				End If
				If MACList.IndexOf(MacAddress) = -1 Then ' only add to list if not duplicate
					MACList.Add(MacAddress)
					FoundDevices.Add(nameAndMac)
					If IsPaused(Main) = False Then 
						CallSub2(Main, "ProgressDialogShow_", "~ device(s) found...".Replace("~", FoundDevices.Size))
					End If
				End If
			End If
		End If
	End If
End Sub

Sub bleEvent_DiscoveryFinished
	Log("DISCOVERY FINISHED")
	If IsPaused(Main) = False Then CallSub(Main, "ProgressDialogHide_")
	If FoundDevices.Size = 0 Then
		ToastMessageShow("No devices found", False)
	Else
		Dim devices As List
		devices.Initialize
		For i = 0 To FoundDevices.Size - 1
			Dim nameAndMac As NameAndMAC
			nameAndMac = FoundDevices.Get(i)
			devices.Add(nameAndMac.Name & " (" & nameAndMac.Mac & ")")
		Next
		Log("CONNECTING TO " & devices.Get(0) )
		If IsPaused(Main) = False Then CallSub2(Main, "SelectDeviceAndConnect", devices)
	End If
End Sub

Sub bleEvent_Connected (Services As Map)
	Log("CONNECTED, " & Services.Size & " services available. Going through the map...")
	GATTServicesMap = Services	
	Dim i, j As Int
	Try
		If ConnectedDevice.Name <> Null Then	'ConnectedDevice.Name
			Starter.AppData.PutSimple("MAC", ConnectedDevice.MAC)
			Starter.AppData.PutSimple("name", ConnectedDevice.Name)
			Starter.lastConnectedMAC = Starter.AppData.GetSimple("MAC") 'ConnectedDevice.MAC
			Starter.lastConnectedName = Starter.AppData.GetSimple("name") 'ConnectedDevice.Name
		Else
			Starter.lastConnectedName = "Aistin Blue"
		End If
	Catch
		Log("Connected Error1" & LastException)
	End Try
	Try
		CallSub(Starter, "CreateLogFile")
	Catch
		Log("Connected Error2" & LastException)
	End Try
	If ConnectState == cstateIdle Then
		ConnectState = cstateConnect
		For i = 0 To GATTServicesMap.Size - 1
			workerService = GATTServicesMap.GetValueAt(i)
			'Log ("workerService " & workerService.Uuid & " is at " & i)
			'FIND BATTERY SERVICE
			If workerService.Uuid.EqualsIgnoreCase("0000180f-0000-1000-8000-00805f9b34fb") Then 
				BatteryService = GATTServicesMap.GetValueAt(i)
				Log ("BatteryService " & BatteryService.Uuid & " found at i=" & i)
			End If
			'FIND nRFUART SERVICE			
			If workerService.Uuid.EqualsIgnoreCase("6e400001-b5a3-f393-e0a9-e50e24dcca9e") Then 
				NRFUartService = GATTServicesMap.GetValueAt(i)
				Log ("NRFUartService " & NRFUartService.Uuid & " found at i=" & i)
			End If
			'FIND DFU SERVICE
			If workerService.Uuid.EqualsIgnoreCase("00001530-1212-efde-1523-785feabcd123") Then
				DFUService =  GATTServicesMap.GetValueAt(i)
				Log ("DFUService " & DFUService.Uuid & " found at i=" & i)
			End If
		Next
		'FIND & LOG DFU CHARACTERISTICs
		For j = 0 To DFUService.GetCharacteristics.Size - 1
			workerCharacteristic = DFUService.GetCharacteristics.GetValueAt(j)
			Log ("DFUCharacteristic " & workerCharacteristic.Uuid & " found at j=" & j)
		Next
		Delay(100, False)		
		'FIND & READ BATTERY LEVEL CHARACTERISTIC
		For j = 0 To BatteryService.GetCharacteristics.Size - 1
			workerCharacteristic = BatteryService.GetCharacteristics.GetValueAt(j)
			If workerCharacteristic.Uuid.EqualsIgnoreCase("00002a19-0000-1000-8000-00805f9b34fb") Then
				BatteryLevelCharacteristic = BatteryService.GetCharacteristics.GetValueAt(j)
				Log ("BatteryLevelCharacteristic " & BatteryLevelCharacteristic.Uuid & " found at j=" & j)
				Manager.ReadCharacteristic(BatteryLevelCharacteristic)
			End If
		Next
		Delay(100, False)
		'FIND nRFUART READ/WRITE CHARACTERISTICs
		For j = 0 To NRFUartService.GetCharacteristics.Size - 1
			workerCharacteristic = NRFUartService.GetCharacteristics.GetValueAt(j)
			If workerCharacteristic.Uuid.EqualsIgnoreCase("6e400003-b5a3-f393-e0a9-e50e24dcca9e") Then
				NRFUartReadCharacteristic = NRFUartService.GetCharacteristics.GetValueAt(j)
				Log ("NRFUartReadCharacteristic " & NRFUartReadCharacteristic.Uuid & " found at j=" & j)
			End If
			If workerCharacteristic.Uuid.EqualsIgnoreCase("6e400002-b5a3-f393-e0a9-e50e24dcca9e") Then
				NRFUartWriteCharacteristic = NRFUartService.GetCharacteristics.GetValueAt(j)
				Log ("NRFUartWriteCharacteristic " & workerCharacteristic.Uuid & " found at j=" & j)
			End If
		Next	
	End If	
	Delay(100, False)
	'ENABLE NOTIFICATIONS FROM nRFU READ CHARACTERISTICS
	Manager.ReadCharacteristic(NRFUartReadCharacteristic)
	Manager.SetCharacteristicNotification(NRFUartReadCharacteristic, True)
	BatteryLevelReader.Enabled = True
	'UPDATE UI
	SensorData.RefreshButtonStatus = "Rescan"
	SensorData.BlueConnectedStatus = "Connected"
	SensorData.ConnectedDeviceName = Starter.lastConnectedName 'ConnectedDevice.Name
	If IsPaused(Main) = False Then CallSub(Main, "ProgressDialogHide_")
	ToastMessageShow("Connected", False)
End Sub

Sub bleEvent_CharacteristicRead (Success As Boolean, Characteristic As BleCharacteristic)
	If Success Then
		If Characteristic.Uuid == "00002a19-0000-1000-8000-00805f9b34fb" Then 'Battery level characteristic
			Log("CharacteristicRead UUID: " & Characteristic.Uuid & " Value: " & Characteristic.GetValue(0))
			SensorData.currentBattery = Characteristic.GetValue(0)
		End If
	End If
End Sub

Sub bleEvent_CharacteristicChanged (Characteristic As BleCharacteristic)
	Dim dataHex As String = ByteConverter.HexFromBytes(Characteristic.GetValue)
	Dim timestamp As String = DateTime.Time(DateTime.Now)	
	Log("dataHex: " & dataHex)
	If Characteristic.Uuid == "6e400003-b5a3-f393-e0a9-e50e24dcca9e" Then 'nRFUart RX characteristic
		Dim sequence As String = dataHex.SubString2(0,2)
		Dim register As String = dataHex.SubString2(2,4)
		Dim value As String
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''		
		Select register
			Case "F1" 'motion
				'accelerometer
				Dim accXYZ As String = ParseData.AccelerationToCSV(dataHex.SubString2(4, 13))
				SensorData.currentAcceleration = ParseData.ParseResultToString2(accXYZ)
				'magnetometer
				Dim magXYZ As String = ParseData.MagnetismToCSV(dataHex.SubString2(14, 23))
				SensorData.currentMagnetism = ParseData.ParseResultToString2(magXYZ)
				'gyroscope
				Dim gyrXYZ As String = ParseData.RotationToCSV(dataHex.SubString2(24, 33))
				SensorData.currentRotation = ParseData.ParseResultToString2(gyrXYZ)
				'log
				If (Starter.logFlag) Then 
					value = timestamp &";"& sequence &";"& register &";"& accXYZ &";"& magXYZ &";"& gyrXYZ
					If (Starter.CSVstore) Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If (Starter.SQLstore) Then CallSubDelayed2(Starter, "StoreData", value)
				End If
				
			Case "F2" 'ambient
				'air pressure
				Dim baro As String = ParseData.parseHexToAirPressure2(dataHex.SubString2(4, 8))
				SensorData.currentAirPressure = ParseData.ParseResultToString(baro)
				'air temperature (from barometer)
				Dim tempB As String = ParseData.ParseHexToFloat(dataHex.SubString2(8, 12))
				SensorData.currentAirTemp = ParseData.ParseResultToString(tempB)
				'air humidity
				Dim humi As String = ParseData.ParseHexToFloat(dataHex.SubString2(12, 16))
				SensorData.currentAirHumidity = ParseData.ParseResultToString(humi)
				'air temperature (from humidity sensor)
				'Dim tempH As String = ParseData.ParseHexToFloat(dataHex.SubString2(16, 20))
				'SensorData.currentAirTemp = ParseData.ParseResultToString(tempH)
				'log
				If (Starter.logFlag) Then
					value = timestamp &";"& sequence &";"& register &";"& baro &";"& tempB &";"& humi '&";"& tempH
					If (Starter.CSVstore) Then CallSubDelayed2(Starter, "WriteDataToFile", value)
					If (Starter.SQLstore) Then CallSubDelayed2(Starter, "StoreData", value)
				End If		
		End Select
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	End If

End Sub

Sub bleEvent_CharacteristicWrite (Success As Boolean, Characteristic As BleCharacteristic)
	If Characteristic.Uuid == "6e400002-b5a3-f393-e0a9-e50e24dcca9e" Then 'nRFUart TX characteristic
		Log("CharacteristicWrite Success: " & Success & ",  Value After write: " & Characteristic.GetIntValue(Characteristic.FORMAT_UINT8,0))
	End If	
End Sub

Sub bleEvent_Disconnected
	Log ("Disconnect")
	SensorData.ConnectedDeviceName = "Aistin Blue"
	SensorData.BlueConnectedStatus = "Disconnected"
	SensorData.RefreshButtonStatus = "Scan"	
	SensorData.currentAirTemp = "---"	
	SensorData.currentAirHumidity = "---"
	SensorData.currentAirPressure = "---"
	SensorData.currentAcceleration = "---"
	SensorData.currentMagnetism = "---"
	SensorData.currentRotation = "---"
	SensorData.currentBattery = "---"
	If (Main.closedByUser) Then
		DisconnectBLE
	Else
		ConnectToDevice
	End If
End Sub

Sub ReadBatteryLevel_Tick
	Manager.ReadCharacteristic(BatteryLevelCharacteristic)
End Sub

Sub DisconnectBLE
	Manager.Disconnect
	Delay(250, True)
	Manager.Close
	ConnectState = cstateIdle
	ToastMessageShow("Disconnected", False)
End Sub

Sub ConnectToDevice
	ToastMessageShow("Searching for devices...", False)
	FoundDevices.Initialize
	MACList.Initialize
	Try
		If BT.IsEnabled = False Then
			BT.Enable
			Delay(500, True)
		End If
		ConnectState = cstateIdle
		If (Manager.BleSupported) Then
			Manager.Scan(8000, Null) ' scan to discover all devices
		Else
			DisconnectBLE
			ToastMessageShow("No devices found", True)
		End If
	Catch
		Log("ConnectToDevice Error" & LastException)
	End Try
End Sub

Sub Delay (nMilliSecond As Long, EnableEvents As Boolean)
	Dim nBeginTime, nEndTime As Long
	nEndTime = DateTime.Now + nMilliSecond
	nBeginTime = DateTime.Now
	Do While nBeginTime < nEndTime
		nBeginTime = DateTime.Now
		If nEndTime < nBeginTime Then Return
		If (EnableEvents) Then DoEvents
	Loop
End Sub

' Helper function to dump the properties of the connection
Sub BleCharacteristicPermissionsToString (Characteristic As BleCharacteristic) As String
	Dim Output As StringBuilder
	Output.Initialize
	
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_READ) <> 0 Then
		Output.Append("Read ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_READ_ENCRYPTED) <> 0 Then
		Output.Append("READ_ENCRYPTED ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_READ_ENCRYPTED_MITM)  <> 0 Then
		Output.Append("Read_E_MITM ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE) <> 0 Then
		Output.Append("Write ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE_ENCRYPTED)  <> 0 Then
		Output.Append("Write_ENCRYPTED ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE) <> 0 Then
		Output.Append("Write_E_MITM ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE_SIGNED) <> 0 Then
		Output.Append("Write_SIGNED ")
	End If
	If Bit.And(Characteristic.Permissions , Characteristic.PERMISSION_WRITE_SIGNED_MITM) <> 0 Then
		Output.Append("Write_S_MITM ")
	End If

   Return Output.ToString

End Sub
' Helper function to dump the properties of the connection
Sub BleCharacteristicPropertiesToString (Characteristic As BleCharacteristic) As String
	Dim Output As StringBuilder
	Output.Initialize
	
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_BROADCAST) <> 0 Then
		Output.Append("BROADCAST ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_EXTENDED_PROPS) <> 0 Then
		Output.Append("EXTENDED_PROPS ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_INDICATE) <> 0 Then
		Output.Append("EXTENDED_INDICATE ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_NOTIFY) <> 0 Then
		Output.Append("NOTIFY ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_READ) <> 0 Then
		Output.Append("READ ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_SIGNED_WRITE) <> 0 Then
		Output.Append("SIGNED_WRITE ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_WRITE) <> 0 Then
		Output.Append("WRITE ")
	End If
	If Bit.And(Characteristic.Properties , Characteristic.PROPERTY_WRITE_NO_RESPONSE) <> 0 Then
		Output.Append("WRITE_NO_RESPONSE ")
	End If
	
	Return Output.ToString
	
End Sub
' Helper function to dump the properties of the connection
Sub BleCharacteristicWriteTypeToString (Characteristic As BleCharacteristic) As String
	Dim Output As StringBuilder
	Output.Initialize
	
	If Bit.And(Characteristic.WriteType , Characteristic.WRITE_TYPE_DEFAULT) <> 0 Then
		Output.Append("TYPE_DEFAULT ")
	End If
	If Bit.And(Characteristic.WriteType , Characteristic.WRITE_TYPE_NO_RESPONSE) <> 0 Then
		Output.Append("TYPE_NO_RESPONSE ")
	End If
	If Bit.And(Characteristic.WriteType , Characteristic.WRITE_TYPE_SIGNED) <> 0 Then
		Output.Append("TYPE_SIGNED ")
	End If

   Return Output.ToString
End Sub