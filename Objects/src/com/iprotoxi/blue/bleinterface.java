package com.iprotoxi.blue;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ServiceHelper;
import anywheresoftware.b4a.debug.*;

public class bleinterface extends android.app.Service {
	public static class bleinterface_BR extends android.content.BroadcastReceiver {

		@Override
		public void onReceive(android.content.Context context, android.content.Intent intent) {
			android.content.Intent in = new android.content.Intent(context, bleinterface.class);
			if (intent != null)
				in.putExtra("b4a_internal_intent", intent);
			context.startService(in);
		}

	}
    static bleinterface mostCurrent;
	public static BA processBA;
    private ServiceHelper _service;
    public static Class<?> getObject() {
		return bleinterface.class;
	}
	@Override
	public void onCreate() {
        mostCurrent = this;
        if (processBA == null) {
		    processBA = new BA(this, null, null, "com.iprotoxi.blue", "com.iprotoxi.blue.bleinterface");
            if (BA.isShellModeRuntimeCheck(processBA)) {
                processBA.raiseEvent2(null, true, "SHELL", false);
		    }
            try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            processBA.loadHtSubs(this.getClass());
            ServiceHelper.init();
        }
        _service = new ServiceHelper(this);
        processBA.service = this;
        processBA.setActivityPaused(false);
        if (BA.isShellModeRuntimeCheck(processBA)) {
			processBA.raiseEvent2(null, true, "CREATE", true, "com.iprotoxi.blue.bleinterface", processBA, _service);
		}
        if (!false && ServiceHelper.StarterHelper.startFromServiceCreate(processBA, false) == false) {
				
		}
		else {
            BA.LogInfo("** Service (bleinterface) Create **");
            processBA.raiseEvent(null, "service_create");
        }
        processBA.runHook("oncreate", this, null);
        if (false) {
			if (ServiceHelper.StarterHelper.waitForLayout != null)
				BA.handler.post(ServiceHelper.StarterHelper.waitForLayout);
		}
    }
		@Override
	public void onStart(android.content.Intent intent, int startId) {
		onStartCommand(intent, 0, 0);
    }
    @Override
    public int onStartCommand(final android.content.Intent intent, int flags, int startId) {
    	if (ServiceHelper.StarterHelper.onStartCommand(processBA))
			handleStart(intent);
		else {
			ServiceHelper.StarterHelper.waitForLayout = new Runnable() {
				public void run() {
                    BA.LogInfo("** Service (bleinterface) Create **");
                    processBA.raiseEvent(null, "service_create");
					handleStart(intent);
				}
			};
		}
        processBA.runHook("onstartcommand", this, new Object[] {intent, flags, startId});
		return android.app.Service.START_NOT_STICKY;
    }
    private void handleStart(android.content.Intent intent) {
    	BA.LogInfo("** Service (bleinterface) Start **");
    	java.lang.reflect.Method startEvent = processBA.htSubs.get("service_start");
    	if (startEvent != null) {
    		if (startEvent.getParameterTypes().length > 0) {
    			anywheresoftware.b4a.objects.IntentWrapper iw = new anywheresoftware.b4a.objects.IntentWrapper();
    			if (intent != null) {
    				if (intent.hasExtra("b4a_internal_intent"))
    					iw.setObject((android.content.Intent) intent.getParcelableExtra("b4a_internal_intent"));
    				else
    					iw.setObject(intent);
    			}
    			processBA.raiseEvent(null, "service_start", iw);
    		}
    		else {
    			processBA.raiseEvent(null, "service_start");
    		}
    	}
    }
	@Override
	public android.os.IBinder onBind(android.content.Intent intent) {
		return null;
	}
	@Override
	public void onDestroy() {
        BA.LogInfo("** Service (bleinterface) Destroy **");
		processBA.raiseEvent(null, "service_destroy");
        processBA.service = null;
		mostCurrent = null;
		processBA.setActivityPaused(true);
        processBA.runHook("ondestroy", this, null);
	}
public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.Serial.BluetoothAdmin _bt = null;
public static anywheresoftware.b4a.objects.BleManager _manager = null;
public static anywheresoftware.b4a.objects.collections.Map _gattservicesmap = null;
public static anywheresoftware.b4a.objects.BleManager.GattServiceWrapper _workerservice = null;
public static anywheresoftware.b4a.objects.BleManager.GattCharacteristic _workercharacteristic = null;
public static anywheresoftware.b4a.objects.BleManager.GattServiceWrapper _batteryservice = null;
public static anywheresoftware.b4a.objects.BleManager.GattCharacteristic _batterylevelcharacteristic = null;
public static anywheresoftware.b4a.objects.BleManager.GattServiceWrapper _nrfuartservice = null;
public static anywheresoftware.b4a.objects.BleManager.GattCharacteristic _nrfuartreadcharacteristic = null;
public static anywheresoftware.b4a.objects.BleManager.GattCharacteristic _nrfuartwritecharacteristic = null;
public static anywheresoftware.b4a.objects.BleManager.GattServiceWrapper _dfuservice = null;
public static anywheresoftware.b4a.objects.Timer _batterylevelreader = null;
public static int _connectstate = 0;
public static int _cstateidle = 0;
public static int _cstateconnect = 0;
public static anywheresoftware.b4a.agraham.byteconverter.ByteConverter _byteconverter = null;
public static com.iprotoxi.blue.bleinterface._nameandmac _connecteddevice = null;
public static anywheresoftware.b4a.objects.collections.List _founddevices = null;
public static anywheresoftware.b4a.objects.collections.List _maclist = null;
public static boolean _connecttolastflag = false;
public com.iprotoxi.blue.main _main = null;
public com.iprotoxi.blue.parsedata _parsedata = null;
public com.iprotoxi.blue.sensordata _sensordata = null;
public com.iprotoxi.blue.starter _starter = null;
public com.iprotoxi.blue.ble2interface _ble2interface = null;
public static class _nameandmac{
public boolean IsInitialized;
public String Name;
public String MAC;
public void Initialize() {
IsInitialized = true;
Name = "";
MAC = "";
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static String  _blecharacteristicpermissionstostring(anywheresoftware.b4a.objects.BleManager.GattCharacteristic _characteristic) throws Exception{
anywheresoftware.b4a.keywords.StringBuilderWrapper _output = null;
 //BA.debugLineNum = 331;BA.debugLine="Sub BleCharacteristicPermissionsToString (Characte";
 //BA.debugLineNum = 332;BA.debugLine="Dim Output As StringBuilder";
_output = new anywheresoftware.b4a.keywords.StringBuilderWrapper();
 //BA.debugLineNum = 333;BA.debugLine="Output.Initialize";
_output.Initialize();
 //BA.debugLineNum = 335;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_READ)!=0) { 
 //BA.debugLineNum = 336;BA.debugLine="Output.Append(\"Read \")";
_output.Append("Read ");
 };
 //BA.debugLineNum = 338;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_READ_ENCRYPTED)!=0) { 
 //BA.debugLineNum = 339;BA.debugLine="Output.Append(\"READ_ENCRYPTED \")";
_output.Append("READ_ENCRYPTED ");
 };
 //BA.debugLineNum = 341;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_READ_ENCRYPTED_MITM)!=0) { 
 //BA.debugLineNum = 342;BA.debugLine="Output.Append(\"Read_E_MITM \")";
_output.Append("Read_E_MITM ");
 };
 //BA.debugLineNum = 344;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_WRITE)!=0) { 
 //BA.debugLineNum = 345;BA.debugLine="Output.Append(\"Write \")";
_output.Append("Write ");
 };
 //BA.debugLineNum = 347;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_WRITE_ENCRYPTED)!=0) { 
 //BA.debugLineNum = 348;BA.debugLine="Output.Append(\"Write_ENCRYPTED \")";
_output.Append("Write_ENCRYPTED ");
 };
 //BA.debugLineNum = 350;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_WRITE)!=0) { 
 //BA.debugLineNum = 351;BA.debugLine="Output.Append(\"Write_E_MITM \")";
_output.Append("Write_E_MITM ");
 };
 //BA.debugLineNum = 353;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_WRITE_SIGNED)!=0) { 
 //BA.debugLineNum = 354;BA.debugLine="Output.Append(\"Write_SIGNED \")";
_output.Append("Write_SIGNED ");
 };
 //BA.debugLineNum = 356;BA.debugLine="If Bit.And(Characteristic.Permissions , Character";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getPermissions(),_characteristic.PERMISSION_WRITE_SIGNED_MITM)!=0) { 
 //BA.debugLineNum = 357;BA.debugLine="Output.Append(\"Write_S_MITM \")";
_output.Append("Write_S_MITM ");
 };
 //BA.debugLineNum = 360;BA.debugLine="Return Output.ToString";
if (true) return _output.ToString();
 //BA.debugLineNum = 362;BA.debugLine="End Sub";
return "";
}
public static String  _blecharacteristicpropertiestostring(anywheresoftware.b4a.objects.BleManager.GattCharacteristic _characteristic) throws Exception{
anywheresoftware.b4a.keywords.StringBuilderWrapper _output = null;
 //BA.debugLineNum = 364;BA.debugLine="Sub BleCharacteristicPropertiesToString (Character";
 //BA.debugLineNum = 365;BA.debugLine="Dim Output As StringBuilder";
_output = new anywheresoftware.b4a.keywords.StringBuilderWrapper();
 //BA.debugLineNum = 366;BA.debugLine="Output.Initialize";
_output.Initialize();
 //BA.debugLineNum = 368;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_BROADCAST)!=0) { 
 //BA.debugLineNum = 369;BA.debugLine="Output.Append(\"BROADCAST \")";
_output.Append("BROADCAST ");
 };
 //BA.debugLineNum = 371;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_EXTENDED_PROPS)!=0) { 
 //BA.debugLineNum = 372;BA.debugLine="Output.Append(\"EXTENDED_PROPS \")";
_output.Append("EXTENDED_PROPS ");
 };
 //BA.debugLineNum = 374;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_INDICATE)!=0) { 
 //BA.debugLineNum = 375;BA.debugLine="Output.Append(\"EXTENDED_INDICATE \")";
_output.Append("EXTENDED_INDICATE ");
 };
 //BA.debugLineNum = 377;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_NOTIFY)!=0) { 
 //BA.debugLineNum = 378;BA.debugLine="Output.Append(\"NOTIFY \")";
_output.Append("NOTIFY ");
 };
 //BA.debugLineNum = 380;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_READ)!=0) { 
 //BA.debugLineNum = 381;BA.debugLine="Output.Append(\"READ \")";
_output.Append("READ ");
 };
 //BA.debugLineNum = 383;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_SIGNED_WRITE)!=0) { 
 //BA.debugLineNum = 384;BA.debugLine="Output.Append(\"SIGNED_WRITE \")";
_output.Append("SIGNED_WRITE ");
 };
 //BA.debugLineNum = 386;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_WRITE)!=0) { 
 //BA.debugLineNum = 387;BA.debugLine="Output.Append(\"WRITE \")";
_output.Append("WRITE ");
 };
 //BA.debugLineNum = 389;BA.debugLine="If Bit.And(Characteristic.Properties , Characteri";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getProperties(),_characteristic.PROPERTY_WRITE_NO_RESPONSE)!=0) { 
 //BA.debugLineNum = 390;BA.debugLine="Output.Append(\"WRITE_NO_RESPONSE \")";
_output.Append("WRITE_NO_RESPONSE ");
 };
 //BA.debugLineNum = 393;BA.debugLine="Return Output.ToString";
if (true) return _output.ToString();
 //BA.debugLineNum = 395;BA.debugLine="End Sub";
return "";
}
public static String  _blecharacteristicwritetypetostring(anywheresoftware.b4a.objects.BleManager.GattCharacteristic _characteristic) throws Exception{
anywheresoftware.b4a.keywords.StringBuilderWrapper _output = null;
 //BA.debugLineNum = 397;BA.debugLine="Sub BleCharacteristicWriteTypeToString (Characteri";
 //BA.debugLineNum = 398;BA.debugLine="Dim Output As StringBuilder";
_output = new anywheresoftware.b4a.keywords.StringBuilderWrapper();
 //BA.debugLineNum = 399;BA.debugLine="Output.Initialize";
_output.Initialize();
 //BA.debugLineNum = 401;BA.debugLine="If Bit.And(Characteristic.WriteType , Characteris";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getWriteType(),_characteristic.WRITE_TYPE_DEFAULT)!=0) { 
 //BA.debugLineNum = 402;BA.debugLine="Output.Append(\"TYPE_DEFAULT \")";
_output.Append("TYPE_DEFAULT ");
 };
 //BA.debugLineNum = 404;BA.debugLine="If Bit.And(Characteristic.WriteType , Characteris";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getWriteType(),_characteristic.WRITE_TYPE_NO_RESPONSE)!=0) { 
 //BA.debugLineNum = 405;BA.debugLine="Output.Append(\"TYPE_NO_RESPONSE \")";
_output.Append("TYPE_NO_RESPONSE ");
 };
 //BA.debugLineNum = 407;BA.debugLine="If Bit.And(Characteristic.WriteType , Characteris";
if (anywheresoftware.b4a.keywords.Common.Bit.And(_characteristic.getWriteType(),_characteristic.WRITE_TYPE_SIGNED)!=0) { 
 //BA.debugLineNum = 408;BA.debugLine="Output.Append(\"TYPE_SIGNED \")";
_output.Append("TYPE_SIGNED ");
 };
 //BA.debugLineNum = 411;BA.debugLine="Return Output.ToString";
if (true) return _output.ToString();
 //BA.debugLineNum = 412;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_characteristicchanged(anywheresoftware.b4a.objects.BleManager.GattCharacteristic _characteristic) throws Exception{
String _datahex = "";
String _timestamp = "";
String _sequence = "";
String _register = "";
String _value = "";
String _accxyz = "";
String _magxyz = "";
String _gyrxyz = "";
String _baro = "";
String _tempb = "";
String _humi = "";
 //BA.debugLineNum = 209;BA.debugLine="Sub bleEvent_CharacteristicChanged (Characteristic";
 //BA.debugLineNum = 210;BA.debugLine="Dim dataHex As String = ByteConverter.HexFromByte";
_datahex = _byteconverter.HexFromBytes(_characteristic.GetValue());
 //BA.debugLineNum = 211;BA.debugLine="Dim timestamp As String = DateTime.Time(DateTime.";
_timestamp = anywheresoftware.b4a.keywords.Common.DateTime.Time(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 212;BA.debugLine="Log(\"dataHex: \" & dataHex)";
anywheresoftware.b4a.keywords.Common.Log("dataHex: "+_datahex);
 //BA.debugLineNum = 213;BA.debugLine="If Characteristic.Uuid == \"6e400003-b5a3-f393-e0a";
if ((_characteristic.getUuid()).equals("6e400003-b5a3-f393-e0a9-e50e24dcca9e")) { 
 //BA.debugLineNum = 214;BA.debugLine="Dim sequence As String = dataHex.SubString2(0,2)";
_sequence = _datahex.substring((int) (0),(int) (2));
 //BA.debugLineNum = 215;BA.debugLine="Dim register As String = dataHex.SubString2(2,4)";
_register = _datahex.substring((int) (2),(int) (4));
 //BA.debugLineNum = 216;BA.debugLine="Dim value As String";
_value = "";
 //BA.debugLineNum = 218;BA.debugLine="Select register";
switch (BA.switchObjectToInt(_register,"F1","F2")) {
case 0:
 //BA.debugLineNum = 221;BA.debugLine="Dim accXYZ As String = ParseData.AccelerationT";
_accxyz = mostCurrent._parsedata._accelerationtocsv(processBA,_datahex.substring((int) (4),(int) (13)));
 //BA.debugLineNum = 222;BA.debugLine="SensorData.currentAcceleration = ParseData.Par";
mostCurrent._sensordata._currentacceleration = mostCurrent._parsedata._parseresulttostring2(processBA,_accxyz);
 //BA.debugLineNum = 224;BA.debugLine="Dim magXYZ As String = ParseData.MagnetismToCS";
_magxyz = mostCurrent._parsedata._magnetismtocsv(processBA,_datahex.substring((int) (14),(int) (23)));
 //BA.debugLineNum = 225;BA.debugLine="SensorData.currentMagnetism = ParseData.ParseR";
mostCurrent._sensordata._currentmagnetism = mostCurrent._parsedata._parseresulttostring2(processBA,_magxyz);
 //BA.debugLineNum = 227;BA.debugLine="Dim gyrXYZ As String = ParseData.RotationToCSV";
_gyrxyz = mostCurrent._parsedata._rotationtocsv(processBA,_datahex.substring((int) (24),(int) (33)));
 //BA.debugLineNum = 228;BA.debugLine="SensorData.currentRotation = ParseData.ParseRe";
mostCurrent._sensordata._currentrotation = mostCurrent._parsedata._parseresulttostring2(processBA,_gyrxyz);
 //BA.debugLineNum = 230;BA.debugLine="If (Starter.logFlag) Then";
if ((mostCurrent._starter._logflag)) { 
 //BA.debugLineNum = 231;BA.debugLine="value = timestamp &\";\"& sequence &\";\"& regist";
_value = _timestamp+";"+_sequence+";"+_register+";"+_accxyz+";"+_magxyz+";"+_gyrxyz;
 //BA.debugLineNum = 232;BA.debugLine="If (Starter.CSVstore) Then CallSubDelayed2(St";
if ((mostCurrent._starter._csvstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"WriteDataToFile",(Object)(_value));};
 //BA.debugLineNum = 233;BA.debugLine="If (Starter.SQLstore) Then CallSubDelayed2(St";
if ((mostCurrent._starter._sqlstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"StoreData",(Object)(_value));};
 };
 break;
case 1:
 //BA.debugLineNum = 238;BA.debugLine="Dim baro As String = ParseData.parseHexToAirPr";
_baro = BA.NumberToString(mostCurrent._parsedata._parsehextoairpressure2(processBA,_datahex.substring((int) (4),(int) (8))));
 //BA.debugLineNum = 239;BA.debugLine="SensorData.currentAirPressure = ParseData.Pars";
mostCurrent._sensordata._currentairpressure = mostCurrent._parsedata._parseresulttostring(processBA,(float)(Double.parseDouble(_baro)));
 //BA.debugLineNum = 241;BA.debugLine="Dim tempB As String = ParseData.ParseHexToFloa";
_tempb = BA.NumberToString(mostCurrent._parsedata._parsehextofloat(processBA,_datahex.substring((int) (8),(int) (12))));
 //BA.debugLineNum = 242;BA.debugLine="SensorData.currentAirTemp = ParseData.ParseRes";
mostCurrent._sensordata._currentairtemp = mostCurrent._parsedata._parseresulttostring(processBA,(float)(Double.parseDouble(_tempb)));
 //BA.debugLineNum = 244;BA.debugLine="Dim humi As String = ParseData.ParseHexToFloat";
_humi = BA.NumberToString(mostCurrent._parsedata._parsehextofloat(processBA,_datahex.substring((int) (12),(int) (16))));
 //BA.debugLineNum = 245;BA.debugLine="SensorData.currentAirHumidity = ParseData.Pars";
mostCurrent._sensordata._currentairhumidity = mostCurrent._parsedata._parseresulttostring(processBA,(float)(Double.parseDouble(_humi)));
 //BA.debugLineNum = 250;BA.debugLine="If (Starter.logFlag) Then";
if ((mostCurrent._starter._logflag)) { 
 //BA.debugLineNum = 251;BA.debugLine="value = timestamp &\";\"& sequence &\";\"& regist";
_value = _timestamp+";"+_sequence+";"+_register+";"+_baro+";"+_tempb+";"+_humi;
 //BA.debugLineNum = 252;BA.debugLine="If (Starter.CSVstore) Then CallSubDelayed2(St";
if ((mostCurrent._starter._csvstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"WriteDataToFile",(Object)(_value));};
 //BA.debugLineNum = 253;BA.debugLine="If (Starter.SQLstore) Then CallSubDelayed2(St";
if ((mostCurrent._starter._sqlstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"StoreData",(Object)(_value));};
 };
 break;
}
;
 };
 //BA.debugLineNum = 259;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_characteristicread(boolean _success,anywheresoftware.b4a.objects.BleManager.GattCharacteristic _characteristic) throws Exception{
 //BA.debugLineNum = 200;BA.debugLine="Sub bleEvent_CharacteristicRead (Success As Boolea";
 //BA.debugLineNum = 201;BA.debugLine="If Success Then";
if (_success) { 
 //BA.debugLineNum = 202;BA.debugLine="If Characteristic.Uuid == \"00002a19-0000-1000-80";
if ((_characteristic.getUuid()).equals("00002a19-0000-1000-8000-00805f9b34fb")) { 
 //BA.debugLineNum = 203;BA.debugLine="Log(\"CharacteristicRead UUID: \" & Characteristi";
anywheresoftware.b4a.keywords.Common.Log("CharacteristicRead UUID: "+_characteristic.getUuid()+" Value: "+BA.NumberToString(_characteristic.GetValue()[(int) (0)]));
 //BA.debugLineNum = 204;BA.debugLine="SensorData.currentBattery = Characteristic.GetV";
mostCurrent._sensordata._currentbattery = BA.NumberToString(_characteristic.GetValue()[(int) (0)]);
 };
 };
 //BA.debugLineNum = 207;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_characteristicwrite(boolean _success,anywheresoftware.b4a.objects.BleManager.GattCharacteristic _characteristic) throws Exception{
 //BA.debugLineNum = 261;BA.debugLine="Sub bleEvent_CharacteristicWrite (Success As Boole";
 //BA.debugLineNum = 262;BA.debugLine="If Characteristic.Uuid == \"6e400002-b5a3-f393-e0a";
if ((_characteristic.getUuid()).equals("6e400002-b5a3-f393-e0a9-e50e24dcca9e")) { 
 //BA.debugLineNum = 263;BA.debugLine="Log(\"CharacteristicWrite Success: \" & Success &";
anywheresoftware.b4a.keywords.Common.Log("CharacteristicWrite Success: "+BA.ObjectToString(_success)+",  Value After write: "+BA.NumberToString(_characteristic.GetIntValue(_characteristic.FORMAT_UINT8,(int) (0))));
 };
 //BA.debugLineNum = 265;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_connected(anywheresoftware.b4a.objects.collections.Map _services) throws Exception{
int _i = 0;
int _j = 0;
 //BA.debugLineNum = 116;BA.debugLine="Sub bleEvent_Connected (Services As Map)";
 //BA.debugLineNum = 117;BA.debugLine="Log(\"CONNECTED, \" & Services.Size & \" services av";
anywheresoftware.b4a.keywords.Common.Log("CONNECTED, "+BA.NumberToString(_services.getSize())+" services available. Going through the map...");
 //BA.debugLineNum = 118;BA.debugLine="GATTServicesMap = Services";
_gattservicesmap = _services;
 //BA.debugLineNum = 119;BA.debugLine="Dim i, j As Int";
_i = 0;
_j = 0;
 //BA.debugLineNum = 120;BA.debugLine="Try";
try { //BA.debugLineNum = 121;BA.debugLine="If ConnectedDevice.Name <> Null Then	'ConnectedD";
if (_connecteddevice.Name!= null) { 
 //BA.debugLineNum = 122;BA.debugLine="Starter.AppData.PutSimple(\"MAC\", ConnectedDevic";
mostCurrent._starter._appdata._putsimple("MAC",(Object)(_connecteddevice.MAC));
 //BA.debugLineNum = 123;BA.debugLine="Starter.AppData.PutSimple(\"name\", ConnectedDevi";
mostCurrent._starter._appdata._putsimple("name",(Object)(_connecteddevice.Name));
 //BA.debugLineNum = 124;BA.debugLine="Starter.lastConnectedMAC = Starter.AppData.GetS";
mostCurrent._starter._lastconnectedmac = mostCurrent._starter._appdata._getsimple("MAC");
 //BA.debugLineNum = 125;BA.debugLine="Starter.lastConnectedName = Starter.AppData.Get";
mostCurrent._starter._lastconnectedname = mostCurrent._starter._appdata._getsimple("name");
 }else {
 //BA.debugLineNum = 127;BA.debugLine="Starter.lastConnectedName = \"Aistin Blue\"";
mostCurrent._starter._lastconnectedname = "Aistin Blue";
 };
 } 
       catch (Exception e99) {
			processBA.setLastException(e99); //BA.debugLineNum = 130;BA.debugLine="Log(\"Connected Error1\" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("Connected Error1"+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 132;BA.debugLine="Try";
try { //BA.debugLineNum = 133;BA.debugLine="CallSub(Starter, \"CreateLogFile\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._starter.getObject()),"CreateLogFile");
 } 
       catch (Exception e104) {
			processBA.setLastException(e104); //BA.debugLineNum = 135;BA.debugLine="Log(\"Connected Error2\" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("Connected Error2"+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 137;BA.debugLine="If ConnectState == cstateIdle Then";
if (_connectstate==_cstateidle) { 
 //BA.debugLineNum = 138;BA.debugLine="ConnectState = cstateConnect";
_connectstate = _cstateconnect;
 //BA.debugLineNum = 139;BA.debugLine="For i = 0 To GATTServicesMap.Size - 1";
{
final int step108 = 1;
final int limit108 = (int) (_gattservicesmap.getSize()-1);
for (_i = (int) (0); (step108 > 0 && _i <= limit108) || (step108 < 0 && _i >= limit108); _i = ((int)(0 + _i + step108))) {
 //BA.debugLineNum = 140;BA.debugLine="workerService = GATTServicesMap.GetValueAt(i)";
_workerservice.setObject((android.bluetooth.BluetoothGattService)(_gattservicesmap.GetValueAt(_i)));
 //BA.debugLineNum = 143;BA.debugLine="If workerService.Uuid.EqualsIgnoreCase(\"0000180";
if (_workerservice.getUuid().equalsIgnoreCase("0000180f-0000-1000-8000-00805f9b34fb")) { 
 //BA.debugLineNum = 144;BA.debugLine="BatteryService = GATTServicesMap.GetValueAt(i)";
_batteryservice.setObject((android.bluetooth.BluetoothGattService)(_gattservicesmap.GetValueAt(_i)));
 //BA.debugLineNum = 145;BA.debugLine="Log (\"BatteryService \" & BatteryService.Uuid &";
anywheresoftware.b4a.keywords.Common.Log("BatteryService "+_batteryservice.getUuid()+" found at i="+BA.NumberToString(_i));
 };
 //BA.debugLineNum = 148;BA.debugLine="If workerService.Uuid.EqualsIgnoreCase(\"6e40000";
if (_workerservice.getUuid().equalsIgnoreCase("6e400001-b5a3-f393-e0a9-e50e24dcca9e")) { 
 //BA.debugLineNum = 149;BA.debugLine="NRFUartService = GATTServicesMap.GetValueAt(i)";
_nrfuartservice.setObject((android.bluetooth.BluetoothGattService)(_gattservicesmap.GetValueAt(_i)));
 //BA.debugLineNum = 150;BA.debugLine="Log (\"NRFUartService \" & NRFUartService.Uuid &";
anywheresoftware.b4a.keywords.Common.Log("NRFUartService "+_nrfuartservice.getUuid()+" found at i="+BA.NumberToString(_i));
 };
 //BA.debugLineNum = 153;BA.debugLine="If workerService.Uuid.EqualsIgnoreCase(\"0000153";
if (_workerservice.getUuid().equalsIgnoreCase("00001530-1212-efde-1523-785feabcd123")) { 
 //BA.debugLineNum = 154;BA.debugLine="DFUService =  GATTServicesMap.GetValueAt(i)";
_dfuservice.setObject((android.bluetooth.BluetoothGattService)(_gattservicesmap.GetValueAt(_i)));
 //BA.debugLineNum = 155;BA.debugLine="Log (\"DFUService \" & DFUService.Uuid & \" found";
anywheresoftware.b4a.keywords.Common.Log("DFUService "+_dfuservice.getUuid()+" found at i="+BA.NumberToString(_i));
 };
 }
};
 //BA.debugLineNum = 159;BA.debugLine="For j = 0 To DFUService.GetCharacteristics.Size";
{
final int step123 = 1;
final int limit123 = (int) (_dfuservice.GetCharacteristics().getSize()-1);
for (_j = (int) (0); (step123 > 0 && _j <= limit123) || (step123 < 0 && _j >= limit123); _j = ((int)(0 + _j + step123))) {
 //BA.debugLineNum = 160;BA.debugLine="workerCharacteristic = DFUService.GetCharacteri";
_workercharacteristic.setObject((android.bluetooth.BluetoothGattCharacteristic)(_dfuservice.GetCharacteristics().GetValueAt(_j)));
 //BA.debugLineNum = 161;BA.debugLine="Log (\"DFUCharacteristic \" & workerCharacteristi";
anywheresoftware.b4a.keywords.Common.Log("DFUCharacteristic "+_workercharacteristic.getUuid()+" found at j="+BA.NumberToString(_j));
 }
};
 //BA.debugLineNum = 163;BA.debugLine="Delay(100, False)";
_delay((long) (100),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 165;BA.debugLine="For j = 0 To BatteryService.GetCharacteristics.S";
{
final int step128 = 1;
final int limit128 = (int) (_batteryservice.GetCharacteristics().getSize()-1);
for (_j = (int) (0); (step128 > 0 && _j <= limit128) || (step128 < 0 && _j >= limit128); _j = ((int)(0 + _j + step128))) {
 //BA.debugLineNum = 166;BA.debugLine="workerCharacteristic = BatteryService.GetCharac";
_workercharacteristic.setObject((android.bluetooth.BluetoothGattCharacteristic)(_batteryservice.GetCharacteristics().GetValueAt(_j)));
 //BA.debugLineNum = 167;BA.debugLine="If workerCharacteristic.Uuid.EqualsIgnoreCase(\"";
if (_workercharacteristic.getUuid().equalsIgnoreCase("00002a19-0000-1000-8000-00805f9b34fb")) { 
 //BA.debugLineNum = 168;BA.debugLine="BatteryLevelCharacteristic = BatteryService.Ge";
_batterylevelcharacteristic.setObject((android.bluetooth.BluetoothGattCharacteristic)(_batteryservice.GetCharacteristics().GetValueAt(_j)));
 //BA.debugLineNum = 169;BA.debugLine="Log (\"BatteryLevelCharacteristic \" & BatteryLe";
anywheresoftware.b4a.keywords.Common.Log("BatteryLevelCharacteristic "+_batterylevelcharacteristic.getUuid()+" found at j="+BA.NumberToString(_j));
 //BA.debugLineNum = 170;BA.debugLine="Manager.ReadCharacteristic(BatteryLevelCharact";
_manager.ReadCharacteristic(_batterylevelcharacteristic);
 };
 }
};
 //BA.debugLineNum = 173;BA.debugLine="Delay(100, False)";
_delay((long) (100),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 175;BA.debugLine="For j = 0 To NRFUartService.GetCharacteristics.S";
{
final int step137 = 1;
final int limit137 = (int) (_nrfuartservice.GetCharacteristics().getSize()-1);
for (_j = (int) (0); (step137 > 0 && _j <= limit137) || (step137 < 0 && _j >= limit137); _j = ((int)(0 + _j + step137))) {
 //BA.debugLineNum = 176;BA.debugLine="workerCharacteristic = NRFUartService.GetCharac";
_workercharacteristic.setObject((android.bluetooth.BluetoothGattCharacteristic)(_nrfuartservice.GetCharacteristics().GetValueAt(_j)));
 //BA.debugLineNum = 177;BA.debugLine="If workerCharacteristic.Uuid.EqualsIgnoreCase(\"";
if (_workercharacteristic.getUuid().equalsIgnoreCase("6e400003-b5a3-f393-e0a9-e50e24dcca9e")) { 
 //BA.debugLineNum = 178;BA.debugLine="NRFUartReadCharacteristic = NRFUartService.Get";
_nrfuartreadcharacteristic.setObject((android.bluetooth.BluetoothGattCharacteristic)(_nrfuartservice.GetCharacteristics().GetValueAt(_j)));
 //BA.debugLineNum = 179;BA.debugLine="Log (\"NRFUartReadCharacteristic \" & NRFUartRea";
anywheresoftware.b4a.keywords.Common.Log("NRFUartReadCharacteristic "+_nrfuartreadcharacteristic.getUuid()+" found at j="+BA.NumberToString(_j));
 };
 //BA.debugLineNum = 181;BA.debugLine="If workerCharacteristic.Uuid.EqualsIgnoreCase(\"";
if (_workercharacteristic.getUuid().equalsIgnoreCase("6e400002-b5a3-f393-e0a9-e50e24dcca9e")) { 
 //BA.debugLineNum = 182;BA.debugLine="NRFUartWriteCharacteristic = NRFUartService.Ge";
_nrfuartwritecharacteristic.setObject((android.bluetooth.BluetoothGattCharacteristic)(_nrfuartservice.GetCharacteristics().GetValueAt(_j)));
 //BA.debugLineNum = 183;BA.debugLine="Log (\"NRFUartWriteCharacteristic \" & workerCha";
anywheresoftware.b4a.keywords.Common.Log("NRFUartWriteCharacteristic "+_workercharacteristic.getUuid()+" found at j="+BA.NumberToString(_j));
 };
 }
};
 };
 //BA.debugLineNum = 187;BA.debugLine="Delay(100, False)";
_delay((long) (100),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 189;BA.debugLine="Manager.ReadCharacteristic(NRFUartReadCharacteris";
_manager.ReadCharacteristic(_nrfuartreadcharacteristic);
 //BA.debugLineNum = 190;BA.debugLine="Manager.SetCharacteristicNotification(NRFUartRead";
_manager.SetCharacteristicNotification(_nrfuartreadcharacteristic,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 191;BA.debugLine="BatteryLevelReader.Enabled = True";
_batterylevelreader.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 193;BA.debugLine="SensorData.RefreshButtonStatus = \"Rescan\"";
mostCurrent._sensordata._refreshbuttonstatus = "Rescan";
 //BA.debugLineNum = 194;BA.debugLine="SensorData.BlueConnectedStatus = \"Connected\"";
mostCurrent._sensordata._blueconnectedstatus = "Connected";
 //BA.debugLineNum = 195;BA.debugLine="SensorData.ConnectedDeviceName = Starter.lastConn";
mostCurrent._sensordata._connecteddevicename = mostCurrent._starter._lastconnectedname;
 //BA.debugLineNum = 196;BA.debugLine="If IsPaused(Main) = False Then CallSub(Main, \"Pro";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogHide_");};
 //BA.debugLineNum = 197;BA.debugLine="ToastMessageShow(\"Connected\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Connected",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 198;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_devicefound(String _name,String _macaddress) throws Exception{
com.iprotoxi.blue.bleinterface._nameandmac _nameandmac = null;
 //BA.debugLineNum = 74;BA.debugLine="Sub bleEvent_DeviceFound (Name As String, MacAddre";
 //BA.debugLineNum = 75;BA.debugLine="Log(\"DEVICE FOUND: \" & Name & \" (\" & MacAddress &";
anywheresoftware.b4a.keywords.Common.Log("DEVICE FOUND: "+_name+" ("+_macaddress+")");
 //BA.debugLineNum = 76;BA.debugLine="If MacAddress <> Null Then";
if (_macaddress!= null) { 
 //BA.debugLineNum = 77;BA.debugLine="If Name <> Null Then";
if (_name!= null) { 
 //BA.debugLineNum = 78;BA.debugLine="If Name.Contains(\"BTL\") Then";
if (_name.contains("BTL")) { 
 //BA.debugLineNum = 79;BA.debugLine="Dim nameAndMac As NameAndMAC";
_nameandmac = new com.iprotoxi.blue.bleinterface._nameandmac();
 //BA.debugLineNum = 80;BA.debugLine="nameAndMac.Initialize";
_nameandmac.Initialize();
 //BA.debugLineNum = 81;BA.debugLine="nameAndMac.Name = Name";
_nameandmac.Name = _name;
 //BA.debugLineNum = 82;BA.debugLine="nameAndMac.Mac = MacAddress";
_nameandmac.MAC = _macaddress;
 //BA.debugLineNum = 83;BA.debugLine="If (ConnectToLastFlag) Then";
if ((_connecttolastflag)) { 
 //BA.debugLineNum = 84;BA.debugLine="If MacAddress = Starter.lastConnectedMAC Then";
if ((_macaddress).equals(mostCurrent._starter._lastconnectedmac)) { 
_manager.Connect(mostCurrent._starter._lastconnectedmac,anywheresoftware.b4a.keywords.Common.True);};
 };
 //BA.debugLineNum = 86;BA.debugLine="If MACList.IndexOf(MacAddress) = -1 Then ' onl";
if (_maclist.IndexOf((Object)(_macaddress))==-1) { 
 //BA.debugLineNum = 87;BA.debugLine="MACList.Add(MacAddress)";
_maclist.Add((Object)(_macaddress));
 //BA.debugLineNum = 88;BA.debugLine="FoundDevices.Add(nameAndMac)";
_founddevices.Add((Object)(_nameandmac));
 //BA.debugLineNum = 89;BA.debugLine="If IsPaused(Main) = False Then";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 90;BA.debugLine="CallSub2(Main, \"ProgressDialogShow_\", \"~ dev";
anywheresoftware.b4a.keywords.Common.CallSubNew2(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogShow_",(Object)("~ device(s) found...".replace("~",BA.NumberToString(_founddevices.getSize()))));
 };
 };
 };
 };
 };
 //BA.debugLineNum = 96;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_disconnected() throws Exception{
 //BA.debugLineNum = 267;BA.debugLine="Sub bleEvent_Disconnected";
 //BA.debugLineNum = 268;BA.debugLine="Log (\"Disconnect\")";
anywheresoftware.b4a.keywords.Common.Log("Disconnect");
 //BA.debugLineNum = 269;BA.debugLine="SensorData.ConnectedDeviceName = \"Aistin Blue\"";
mostCurrent._sensordata._connecteddevicename = "Aistin Blue";
 //BA.debugLineNum = 270;BA.debugLine="SensorData.BlueConnectedStatus = \"Disconnected\"";
mostCurrent._sensordata._blueconnectedstatus = "Disconnected";
 //BA.debugLineNum = 271;BA.debugLine="SensorData.RefreshButtonStatus = \"Scan\"";
mostCurrent._sensordata._refreshbuttonstatus = "Scan";
 //BA.debugLineNum = 272;BA.debugLine="SensorData.currentAirTemp = \"---\"";
mostCurrent._sensordata._currentairtemp = "---";
 //BA.debugLineNum = 273;BA.debugLine="SensorData.currentAirHumidity = \"---\"";
mostCurrent._sensordata._currentairhumidity = "---";
 //BA.debugLineNum = 274;BA.debugLine="SensorData.currentAirPressure = \"---\"";
mostCurrent._sensordata._currentairpressure = "---";
 //BA.debugLineNum = 275;BA.debugLine="SensorData.currentAcceleration = \"---\"";
mostCurrent._sensordata._currentacceleration = "---";
 //BA.debugLineNum = 276;BA.debugLine="SensorData.currentMagnetism = \"---\"";
mostCurrent._sensordata._currentmagnetism = "---";
 //BA.debugLineNum = 277;BA.debugLine="SensorData.currentRotation = \"---\"";
mostCurrent._sensordata._currentrotation = "---";
 //BA.debugLineNum = 278;BA.debugLine="SensorData.currentBattery = \"---\"";
mostCurrent._sensordata._currentbattery = "---";
 //BA.debugLineNum = 279;BA.debugLine="If (Main.closedByUser) Then";
if ((mostCurrent._main._closedbyuser)) { 
 //BA.debugLineNum = 280;BA.debugLine="DisconnectBLE";
_disconnectble();
 }else {
 //BA.debugLineNum = 282;BA.debugLine="ConnectToDevice";
_connecttodevice();
 };
 //BA.debugLineNum = 284;BA.debugLine="End Sub";
return "";
}
public static String  _bleevent_discoveryfinished() throws Exception{
anywheresoftware.b4a.objects.collections.List _devices = null;
int _i = 0;
com.iprotoxi.blue.bleinterface._nameandmac _nameandmac = null;
 //BA.debugLineNum = 98;BA.debugLine="Sub bleEvent_DiscoveryFinished";
 //BA.debugLineNum = 99;BA.debugLine="Log(\"DISCOVERY FINISHED\")";
anywheresoftware.b4a.keywords.Common.Log("DISCOVERY FINISHED");
 //BA.debugLineNum = 100;BA.debugLine="If IsPaused(Main) = False Then CallSub(Main, \"Pro";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogHide_");};
 //BA.debugLineNum = 101;BA.debugLine="If FoundDevices.Size = 0 Then";
if (_founddevices.getSize()==0) { 
 //BA.debugLineNum = 102;BA.debugLine="ToastMessageShow(\"No devices found\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("No devices found",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 104;BA.debugLine="Dim devices As List";
_devices = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 105;BA.debugLine="devices.Initialize";
_devices.Initialize();
 //BA.debugLineNum = 106;BA.debugLine="For i = 0 To FoundDevices.Size - 1";
{
final int step76 = 1;
final int limit76 = (int) (_founddevices.getSize()-1);
for (_i = (int) (0); (step76 > 0 && _i <= limit76) || (step76 < 0 && _i >= limit76); _i = ((int)(0 + _i + step76))) {
 //BA.debugLineNum = 107;BA.debugLine="Dim nameAndMac As NameAndMAC";
_nameandmac = new com.iprotoxi.blue.bleinterface._nameandmac();
 //BA.debugLineNum = 108;BA.debugLine="nameAndMac = FoundDevices.Get(i)";
_nameandmac = (com.iprotoxi.blue.bleinterface._nameandmac)(_founddevices.Get(_i));
 //BA.debugLineNum = 109;BA.debugLine="devices.Add(nameAndMac.Name & \" (\" & nameAndMac";
_devices.Add((Object)(_nameandmac.Name+" ("+_nameandmac.MAC+")"));
 }
};
 //BA.debugLineNum = 111;BA.debugLine="Log(\"CONNECTING TO \" & devices.Get(0) )";
anywheresoftware.b4a.keywords.Common.Log("CONNECTING TO "+BA.ObjectToString(_devices.Get((int) (0))));
 //BA.debugLineNum = 112;BA.debugLine="If IsPaused(Main) = False Then CallSub2(Main, \"S";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew2(processBA,(Object)(mostCurrent._main.getObject()),"SelectDeviceAndConnect",(Object)(_devices));};
 };
 //BA.debugLineNum = 114;BA.debugLine="End Sub";
return "";
}
public static String  _connecttodevice() throws Exception{
 //BA.debugLineNum = 298;BA.debugLine="Sub ConnectToDevice";
 //BA.debugLineNum = 299;BA.debugLine="ToastMessageShow(\"Searching for devices...\", Fals";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Searching for devices...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 300;BA.debugLine="FoundDevices.Initialize";
_founddevices.Initialize();
 //BA.debugLineNum = 301;BA.debugLine="MACList.Initialize";
_maclist.Initialize();
 //BA.debugLineNum = 302;BA.debugLine="Try";
try { //BA.debugLineNum = 303;BA.debugLine="If BT.IsEnabled = False Then";
if (_bt.IsEnabled()==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 304;BA.debugLine="BT.Enable";
_bt.Enable();
 //BA.debugLineNum = 305;BA.debugLine="Delay(500, True)";
_delay((long) (500),anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 307;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 //BA.debugLineNum = 308;BA.debugLine="If (Manager.BleSupported) Then";
if ((_manager.getBleSupported())) { 
 //BA.debugLineNum = 309;BA.debugLine="Manager.Scan(8000, Null) ' scan to discover all";
_manager.Scan(processBA,(long) (8000),(String[])(anywheresoftware.b4a.keywords.Common.Null));
 }else {
 //BA.debugLineNum = 311;BA.debugLine="DisconnectBLE";
_disconnectble();
 //BA.debugLineNum = 312;BA.debugLine="ToastMessageShow(\"No devices found\", True)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("No devices found",anywheresoftware.b4a.keywords.Common.True);
 };
 } 
       catch (Exception e253) {
			processBA.setLastException(e253); //BA.debugLineNum = 315;BA.debugLine="Log(\"ConnectToDevice Error\" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("ConnectToDevice Error"+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 317;BA.debugLine="End Sub";
return "";
}
public static String  _delay(long _nmillisecond,boolean _enableevents) throws Exception{
long _nbegintime = 0L;
long _nendtime = 0L;
 //BA.debugLineNum = 319;BA.debugLine="Sub Delay (nMilliSecond As Long, EnableEvents As B";
 //BA.debugLineNum = 320;BA.debugLine="Dim nBeginTime, nEndTime As Long";
_nbegintime = 0L;
_nendtime = 0L;
 //BA.debugLineNum = 321;BA.debugLine="nEndTime = DateTime.Now + nMilliSecond";
_nendtime = (long) (anywheresoftware.b4a.keywords.Common.DateTime.getNow()+_nmillisecond);
 //BA.debugLineNum = 322;BA.debugLine="nBeginTime = DateTime.Now";
_nbegintime = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 323;BA.debugLine="Do While nBeginTime < nEndTime";
while (_nbegintime<_nendtime) {
 //BA.debugLineNum = 324;BA.debugLine="nBeginTime = DateTime.Now";
_nbegintime = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 325;BA.debugLine="If nEndTime < nBeginTime Then Return";
if (_nendtime<_nbegintime) { 
if (true) return "";};
 //BA.debugLineNum = 326;BA.debugLine="If (EnableEvents) Then DoEvents";
if ((_enableevents)) { 
anywheresoftware.b4a.keywords.Common.DoEvents();};
 }
;
 //BA.debugLineNum = 328;BA.debugLine="End Sub";
return "";
}
public static String  _disconnectble() throws Exception{
 //BA.debugLineNum = 290;BA.debugLine="Sub DisconnectBLE";
 //BA.debugLineNum = 291;BA.debugLine="Manager.Disconnect";
_manager.Disconnect();
 //BA.debugLineNum = 292;BA.debugLine="Delay(250, True)";
_delay((long) (250),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 293;BA.debugLine="Manager.Close";
_manager.Close();
 //BA.debugLineNum = 294;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 //BA.debugLineNum = 295;BA.debugLine="ToastMessageShow(\"Disconnected\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Disconnected",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 296;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 10;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 14;BA.debugLine="Dim BT As BluetoothAdmin";
_bt = new anywheresoftware.b4a.objects.Serial.BluetoothAdmin();
 //BA.debugLineNum = 15;BA.debugLine="Dim Manager As BleManager";
_manager = new anywheresoftware.b4a.objects.BleManager();
 //BA.debugLineNum = 16;BA.debugLine="Dim GATTServicesMap As Map";
_gattservicesmap = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 18;BA.debugLine="Dim workerService As BleService";
_workerservice = new anywheresoftware.b4a.objects.BleManager.GattServiceWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Dim workerCharacteristic As BleCharacteristic";
_workercharacteristic = new anywheresoftware.b4a.objects.BleManager.GattCharacteristic();
 //BA.debugLineNum = 21;BA.debugLine="Dim BatteryService As BleService";
_batteryservice = new anywheresoftware.b4a.objects.BleManager.GattServiceWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Dim BatteryLevelCharacteristic As BleCharacterist";
_batterylevelcharacteristic = new anywheresoftware.b4a.objects.BleManager.GattCharacteristic();
 //BA.debugLineNum = 24;BA.debugLine="Dim NRFUartService As BleService";
_nrfuartservice = new anywheresoftware.b4a.objects.BleManager.GattServiceWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Dim NRFUartReadCharacteristic As BleCharacteristi";
_nrfuartreadcharacteristic = new anywheresoftware.b4a.objects.BleManager.GattCharacteristic();
 //BA.debugLineNum = 26;BA.debugLine="Dim NRFUartWriteCharacteristic As BleCharacterist";
_nrfuartwritecharacteristic = new anywheresoftware.b4a.objects.BleManager.GattCharacteristic();
 //BA.debugLineNum = 28;BA.debugLine="Dim DFUService As BleService";
_dfuservice = new anywheresoftware.b4a.objects.BleManager.GattServiceWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Dim BatteryLevelReader As Timer";
_batterylevelreader = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 32;BA.debugLine="Dim ConnectState As Int";
_connectstate = 0;
 //BA.debugLineNum = 33;BA.debugLine="Dim cstateIdle As Int = 0";
_cstateidle = (int) (0);
 //BA.debugLineNum = 35;BA.debugLine="Dim cstateConnect As Int = 2";
_cstateconnect = (int) (2);
 //BA.debugLineNum = 40;BA.debugLine="Dim ByteConverter As ByteConverter";
_byteconverter = new anywheresoftware.b4a.agraham.byteconverter.ByteConverter();
 //BA.debugLineNum = 42;BA.debugLine="Type NameAndMAC (Name As String, MAC As String)";
;
 //BA.debugLineNum = 43;BA.debugLine="Dim ConnectedDevice As NameAndMAC";
_connecteddevice = new com.iprotoxi.blue.bleinterface._nameandmac();
 //BA.debugLineNum = 44;BA.debugLine="Dim FoundDevices As List";
_founddevices = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 45;BA.debugLine="Dim MACList As List";
_maclist = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 46;BA.debugLine="Dim ConnectToLastFlag As Boolean = True";
_connecttolastflag = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 47;BA.debugLine="End Sub";
return "";
}
public static String  _readbatterylevel_tick() throws Exception{
 //BA.debugLineNum = 286;BA.debugLine="Sub ReadBatteryLevel_Tick";
 //BA.debugLineNum = 287;BA.debugLine="Manager.ReadCharacteristic(BatteryLevelCharacteri";
_manager.ReadCharacteristic(_batterylevelcharacteristic);
 //BA.debugLineNum = 288;BA.debugLine="End Sub";
return "";
}
public static String  _service_create() throws Exception{
 //BA.debugLineNum = 49;BA.debugLine="Sub Service_Create";
 //BA.debugLineNum = 50;BA.debugLine="Try";
try { //BA.debugLineNum = 51;BA.debugLine="FoundDevices.Initialize";
_founddevices.Initialize();
 //BA.debugLineNum = 52;BA.debugLine="ConnectedDevice.Initialize";
_connecteddevice.Initialize();
 //BA.debugLineNum = 53;BA.debugLine="GATTServicesMap.Initialize";
_gattservicesmap.Initialize();
 //BA.debugLineNum = 54;BA.debugLine="BT.Initialize(\"btEvent\")";
_bt.Initialize(processBA,"btEvent");
 //BA.debugLineNum = 55;BA.debugLine="Manager.Initialize(\"bleEvent\")";
_manager.Initialize(processBA,"bleEvent");
 //BA.debugLineNum = 56;BA.debugLine="BatteryLevelReader.Initialize(\"ReadBatteryLevel\"";
_batterylevelreader.Initialize(processBA,"ReadBatteryLevel",(long) (3*60000));
 //BA.debugLineNum = 57;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 } 
       catch (Exception e33) {
			processBA.setLastException(e33); //BA.debugLineNum = 59;BA.debugLine="Log(\"Service_Create Error: \" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("Service_Create Error: "+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 //BA.debugLineNum = 60;BA.debugLine="StopService(\"\") 'stop current";
anywheresoftware.b4a.keywords.Common.StopService(processBA,(Object)(""));
 //BA.debugLineNum = 61;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 };
 //BA.debugLineNum = 63;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 //BA.debugLineNum = 64;BA.debugLine="End Sub";
return "";
}
public static String  _service_destroy() throws Exception{
 //BA.debugLineNum = 69;BA.debugLine="Sub Service_Destroy";
 //BA.debugLineNum = 70;BA.debugLine="CallSub(Starter, \"CloseTextwriter\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._starter.getObject()),"CloseTextwriter");
 //BA.debugLineNum = 71;BA.debugLine="StopService(\"\") 'stop self";
anywheresoftware.b4a.keywords.Common.StopService(processBA,(Object)(""));
 //BA.debugLineNum = 72;BA.debugLine="End Sub";
return "";
}
public static String  _service_start(anywheresoftware.b4a.objects.IntentWrapper _startingintent) throws Exception{
 //BA.debugLineNum = 66;BA.debugLine="Sub Service_Start (StartingIntent As Intent)";
 //BA.debugLineNum = 67;BA.debugLine="End Sub";
return "";
}
}
