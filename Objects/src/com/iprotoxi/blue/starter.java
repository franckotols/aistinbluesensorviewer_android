package com.iprotoxi.blue;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ServiceHelper;
import anywheresoftware.b4a.debug.*;

public class starter extends android.app.Service {
	public static class starter_BR extends android.content.BroadcastReceiver {

		@Override
		public void onReceive(android.content.Context context, android.content.Intent intent) {
			android.content.Intent in = new android.content.Intent(context, starter.class);
			if (intent != null)
				in.putExtra("b4a_internal_intent", intent);
			context.startService(in);
		}

	}
    static starter mostCurrent;
	public static BA processBA;
    private ServiceHelper _service;
    public static Class<?> getObject() {
		return starter.class;
	}
	@Override
	public void onCreate() {
        mostCurrent = this;
        if (processBA == null) {
		    processBA = new BA(this, null, null, "com.iprotoxi.blue", "com.iprotoxi.blue.starter");
            if (BA.isShellModeRuntimeCheck(processBA)) {
                processBA.raiseEvent2(null, true, "SHELL", false);
		    }
            try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            processBA.loadHtSubs(this.getClass());
            ServiceHelper.init();
        }
        _service = new ServiceHelper(this);
        processBA.service = this;
        processBA.setActivityPaused(false);
        if (BA.isShellModeRuntimeCheck(processBA)) {
			processBA.raiseEvent2(null, true, "CREATE", true, "com.iprotoxi.blue.starter", processBA, _service);
		}
        if (!true && ServiceHelper.StarterHelper.startFromServiceCreate(processBA, false) == false) {
				
		}
		else {
            BA.LogInfo("** Service (starter) Create **");
            processBA.raiseEvent(null, "service_create");
        }
        processBA.runHook("oncreate", this, null);
        if (true) {
			if (ServiceHelper.StarterHelper.waitForLayout != null)
				BA.handler.post(ServiceHelper.StarterHelper.waitForLayout);
		}
    }
		@Override
	public void onStart(android.content.Intent intent, int startId) {
		onStartCommand(intent, 0, 0);
    }
    @Override
    public int onStartCommand(final android.content.Intent intent, int flags, int startId) {
    	if (ServiceHelper.StarterHelper.onStartCommand(processBA))
			handleStart(intent);
		else {
			ServiceHelper.StarterHelper.waitForLayout = new Runnable() {
				public void run() {
                    BA.LogInfo("** Service (starter) Create **");
                    processBA.raiseEvent(null, "service_create");
					handleStart(intent);
				}
			};
		}
        processBA.runHook("onstartcommand", this, new Object[] {intent, flags, startId});
		return android.app.Service.START_NOT_STICKY;
    }
    private void handleStart(android.content.Intent intent) {
    	BA.LogInfo("** Service (starter) Start **");
    	java.lang.reflect.Method startEvent = processBA.htSubs.get("service_start");
    	if (startEvent != null) {
    		if (startEvent.getParameterTypes().length > 0) {
    			anywheresoftware.b4a.objects.IntentWrapper iw = new anywheresoftware.b4a.objects.IntentWrapper();
    			if (intent != null) {
    				if (intent.hasExtra("b4a_internal_intent"))
    					iw.setObject((android.content.Intent) intent.getParcelableExtra("b4a_internal_intent"));
    				else
    					iw.setObject(intent);
    			}
    			processBA.raiseEvent(null, "service_start", iw);
    		}
    		else {
    			processBA.raiseEvent(null, "service_start");
    		}
    	}
    }
	@Override
	public android.os.IBinder onBind(android.content.Intent intent) {
		return null;
	}
	@Override
	public void onDestroy() {
        BA.LogInfo("** Service (starter) Destroy **");
		processBA.raiseEvent(null, "service_destroy");
        processBA.service = null;
		mostCurrent = null;
		processBA.setActivityPaused(true);
        processBA.runHook("ondestroy", this, null);
	}
public anywheresoftware.b4a.keywords.Common __c = null;
public static com.iprotoxi.blue.keyvaluestore _appdata = null;
public static String _lastconnectedmac = "";
public static String _lastconnectedname = "";
public static int _i = 0;
public static boolean _logflag = false;
public static boolean _sqlstore = false;
public static boolean _csvstore = false;
public static String _filename = "";
public static String _filedir = "";
public static anywheresoftware.b4a.objects.collections.List _data = null;
public static anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper _fileoutput = null;
public static anywheresoftware.b4a.objects.streams.File.TextWriterWrapper _tw = null;
public com.iprotoxi.blue.main _main = null;
public com.iprotoxi.blue.bleinterface _bleinterface = null;
public com.iprotoxi.blue.parsedata _parsedata = null;
public com.iprotoxi.blue.sensordata _sensordata = null;
public com.iprotoxi.blue.ble2interface _ble2interface = null;
public static String  _closetextwriter() throws Exception{
 //BA.debugLineNum = 120;BA.debugLine="Sub CloseTextwriter";
 //BA.debugLineNum = 121;BA.debugLine="If (CSVstore) Then";
if ((_csvstore)) { 
 //BA.debugLineNum = 122;BA.debugLine="CallSubDelayed(Me, \"twClose_\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,starter.getObject(),"twClose_");
 };
 //BA.debugLineNum = 124;BA.debugLine="End Sub";
return "";
}
public static String  _createlogfile() throws Exception{
 //BA.debugLineNum = 74;BA.debugLine="Sub CreateLogFile";
 //BA.debugLineNum = 75;BA.debugLine="DateTime.DateFormat = \"yyyy-MM-dd-HH:mm:ss\"";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyyy-MM-dd-HH:mm:ss");
 //BA.debugLineNum = 76;BA.debugLine="If (logFlag) Then";
if ((_logflag)) { 
 //BA.debugLineNum = 77;BA.debugLine="If (File.ExternalWritable) Then";
if ((anywheresoftware.b4a.keywords.Common.File.getExternalWritable())) { 
 //BA.debugLineNum = 78;BA.debugLine="Try";
try { //BA.debugLineNum = 79;BA.debugLine="twClose_";
_twclose_();
 } 
       catch (Exception e61) {
			processBA.setLastException(e61); //BA.debugLineNum = 81;BA.debugLine="Log(\"CloseTextwriter: \" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("CloseTextwriter: "+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 83;BA.debugLine="CSVstore = True";
_csvstore = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 84;BA.debugLine="FileDir = File.DirDefaultExternal";
_filedir = anywheresoftware.b4a.keywords.Common.File.getDirDefaultExternal();
 //BA.debugLineNum = 85;BA.debugLine="Log(\"FileDir: \" & FileDir)";
anywheresoftware.b4a.keywords.Common.Log("FileDir: "+_filedir);
 //BA.debugLineNum = 86;BA.debugLine="FileName = \"log-\" & DateTime.Date(DateTime.Now)";
_filename = "log-"+anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow())+".csv";
 //BA.debugLineNum = 87;BA.debugLine="Log(\"FileName: \" & FileName)";
anywheresoftware.b4a.keywords.Common.Log("FileName: "+_filename);
 //BA.debugLineNum = 88;BA.debugLine="FileOutput = File.OpenOutput(FileDir, FileName,";
_fileoutput = anywheresoftware.b4a.keywords.Common.File.OpenOutput(_filedir,_filename,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 89;BA.debugLine="tw.Initialize(FileOutput)";
_tw.Initialize((java.io.OutputStream)(_fileoutput.getObject()));
 //BA.debugLineNum = 90;BA.debugLine="Data.Initialize";
_data.Initialize();
 //BA.debugLineNum = 91;BA.debugLine="If (tw.IsInitialized) Then";
if ((_tw.IsInitialized())) { 
 //BA.debugLineNum = 92;BA.debugLine="Log(\"tw.IsInitialized: \" & tw.IsInitialized)";
anywheresoftware.b4a.keywords.Common.Log("tw.IsInitialized: "+BA.ObjectToString(_tw.IsInitialized()));
 }else {
 //BA.debugLineNum = 94;BA.debugLine="CSVstore = False";
_csvstore = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 95;BA.debugLine="ToastMessageShow(\"Cannot log data\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Cannot log data",anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 97;BA.debugLine="If (Data.IsInitialized) Then";
if ((_data.IsInitialized())) { 
 //BA.debugLineNum = 98;BA.debugLine="Log(\"Data.IsInitialized: \" & Data.IsInitialize";
anywheresoftware.b4a.keywords.Common.Log("Data.IsInitialized: "+BA.ObjectToString(_data.IsInitialized()));
 }else {
 //BA.debugLineNum = 100;BA.debugLine="CSVstore = False";
_csvstore = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 101;BA.debugLine="ToastMessageShow(\"Cannot log data\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Cannot log data",anywheresoftware.b4a.keywords.Common.False);
 };
 }else {
 //BA.debugLineNum = 105;BA.debugLine="CSVstore = False";
_csvstore = anywheresoftware.b4a.keywords.Common.False;
 };
 };
 //BA.debugLineNum = 108;BA.debugLine="DateTime.DateFormat = \"yyyy-MM-dd HH:mm:ss\"";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyyy-MM-dd HH:mm:ss");
 //BA.debugLineNum = 109;BA.debugLine="DateTime.TimeFormat = \"yyyy-MM-dd;HH:mm:ss\"";
anywheresoftware.b4a.keywords.Common.DateTime.setTimeFormat("yyyy-MM-dd;HH:mm:ss");
 //BA.debugLineNum = 110;BA.debugLine="Log(\"CSVstore: \" & CSVstore)";
anywheresoftware.b4a.keywords.Common.Log("CSVstore: "+BA.ObjectToString(_csvstore));
 //BA.debugLineNum = 111;BA.debugLine="ToastMessageShow(\"LOGGING DATA\" & CRLF & CRLF & \"";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("LOGGING DATA"+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"Path: "+anywheresoftware.b4a.keywords.Common.CRLF+_filedir+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF+"File: "+anywheresoftware.b4a.keywords.Common.CRLF+_filename,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 112;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim AppData As KeyValueStore";
_appdata = new com.iprotoxi.blue.keyvaluestore();
 //BA.debugLineNum = 10;BA.debugLine="Dim lastConnectedMAC As String";
_lastconnectedmac = "";
 //BA.debugLineNum = 11;BA.debugLine="Dim lastConnectedName As String";
_lastconnectedname = "";
 //BA.debugLineNum = 12;BA.debugLine="Dim i As Int = 0";
_i = (int) (0);
 //BA.debugLineNum = 15;BA.debugLine="Dim logFlag As Boolean = True";
_logflag = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 16;BA.debugLine="Dim SQLstore As Boolean = False";
_sqlstore = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 17;BA.debugLine="Dim CSVstore As Boolean";
_csvstore = false;
 //BA.debugLineNum = 19;BA.debugLine="Dim FileName As String";
_filename = "";
 //BA.debugLineNum = 20;BA.debugLine="Dim FileDir As String";
_filedir = "";
 //BA.debugLineNum = 21;BA.debugLine="Dim Data As List";
_data = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 22;BA.debugLine="Dim FileOutput As OutputStream";
_fileoutput = new anywheresoftware.b4a.objects.streams.File.OutputStreamWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Dim tw As TextWriter";
_tw = new anywheresoftware.b4a.objects.streams.File.TextWriterWrapper();
 //BA.debugLineNum = 24;BA.debugLine="End Sub";
return "";
}
public static String  _service_create() throws Exception{
String _database = "";
 //BA.debugLineNum = 26;BA.debugLine="Sub Service_Create";
 //BA.debugLineNum = 29;BA.debugLine="Dim database As String =  \"aistin.dat\"";
_database = "aistin.dat";
 //BA.debugLineNum = 30;BA.debugLine="Try";
try { //BA.debugLineNum = 31;BA.debugLine="AppData.Initialize(File.DirInternal, database) '";
_appdata._initialize(processBA,anywheresoftware.b4a.keywords.Common.File.getDirInternal(),_database);
 } 
       catch (Exception e19) {
			processBA.setLastException(e19); //BA.debugLineNum = 33;BA.debugLine="Log(\"Service_Create Error: \" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("Service_Create Error: "+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 35;BA.debugLine="DateTime.DateFormat = \"yyyy-MM-dd HH:mm:ss\"";
anywheresoftware.b4a.keywords.Common.DateTime.setDateFormat("yyyy-MM-dd HH:mm:ss");
 //BA.debugLineNum = 36;BA.debugLine="End Sub";
return "";
}
public static String  _service_destroy() throws Exception{
 //BA.debugLineNum = 58;BA.debugLine="Sub Service_Destroy";
 //BA.debugLineNum = 59;BA.debugLine="StopService(BLE2Interface)";
anywheresoftware.b4a.keywords.Common.StopService(processBA,(Object)(mostCurrent._ble2interface.getObject()));
 //BA.debugLineNum = 60;BA.debugLine="CallSubDelayed(Me, \"CloseTextwriter\")";
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,starter.getObject(),"CloseTextwriter");
 //BA.debugLineNum = 61;BA.debugLine="End Sub";
return "";
}
public static String  _service_start(anywheresoftware.b4a.objects.IntentWrapper _startingintent) throws Exception{
 //BA.debugLineNum = 38;BA.debugLine="Sub Service_Start (StartingIntent As Intent)";
 //BA.debugLineNum = 39;BA.debugLine="If AppData.ContainsKey(\"name\") Then";
if (_appdata._containskey("name")) { 
 //BA.debugLineNum = 40;BA.debugLine="Try";
try { //BA.debugLineNum = 41;BA.debugLine="lastConnectedName = AppData.GetSimple(\"name\")";
_lastconnectedname = _appdata._getsimple("name");
 } 
       catch (Exception e28) {
			processBA.setLastException(e28); //BA.debugLineNum = 43;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 //BA.debugLineNum = 44;BA.debugLine="AppData.PutSimple(\"name\", \"\")";
_appdata._putsimple("name",(Object)(""));
 };
 //BA.debugLineNum = 46;BA.debugLine="Try";
try { //BA.debugLineNum = 47;BA.debugLine="lastConnectedMAC = AppData.GetSimple(\"MAC\")";
_lastconnectedmac = _appdata._getsimple("MAC");
 } 
       catch (Exception e34) {
			processBA.setLastException(e34); //BA.debugLineNum = 49;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 //BA.debugLineNum = 50;BA.debugLine="AppData.PutSimple(\"MAC\", \"\")";
_appdata._putsimple("MAC",(Object)(""));
 };
 }else {
 //BA.debugLineNum = 53;BA.debugLine="AppData.PutSimple(\"MAC\", \"\")";
_appdata._putsimple("MAC",(Object)(""));
 //BA.debugLineNum = 54;BA.debugLine="AppData.PutSimple(\"name\", \"\")";
_appdata._putsimple("name",(Object)(""));
 };
 //BA.debugLineNum = 56;BA.debugLine="End Sub";
return "";
}
public static String  _storedata(String _sdata) throws Exception{
 //BA.debugLineNum = 64;BA.debugLine="Sub StoreData(sdata As String)";
 //BA.debugLineNum = 66;BA.debugLine="Try";
try { //BA.debugLineNum = 67;BA.debugLine="AppData.PutSimple(i, sdata)";
_appdata._putsimple(BA.NumberToString(_i),(Object)(_sdata));
 } 
       catch (Exception e50) {
			processBA.setLastException(e50); //BA.debugLineNum = 69;BA.debugLine="Log(\"StoreData Error: \" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("StoreData Error: "+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 71;BA.debugLine="i = i + 1";
_i = (int) (_i+1);
 //BA.debugLineNum = 72;BA.debugLine="End Sub";
return "";
}
public static String  _twclose_() throws Exception{
 //BA.debugLineNum = 126;BA.debugLine="Sub twClose_";
 //BA.debugLineNum = 127;BA.debugLine="tw.Close";
_tw.Close();
 //BA.debugLineNum = 128;BA.debugLine="End Sub";
return "";
}
public static String  _writedatatofile(String _sdata) throws Exception{
 //BA.debugLineNum = 114;BA.debugLine="Sub WriteDataToFile(sdata As String)";
 //BA.debugLineNum = 115;BA.debugLine="If (CSVstore) Then";
if ((_csvstore)) { 
 //BA.debugLineNum = 116;BA.debugLine="tw.WriteLine(sdata)";
_tw.WriteLine(_sdata);
 };
 //BA.debugLineNum = 118;BA.debugLine="End Sub";
return "";
}
}
