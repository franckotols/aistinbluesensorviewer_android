package com.iprotoxi.blue;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ServiceHelper;
import anywheresoftware.b4a.debug.*;

public class ble2interface extends android.app.Service {
	public static class ble2interface_BR extends android.content.BroadcastReceiver {

		@Override
		public void onReceive(android.content.Context context, android.content.Intent intent) {
			android.content.Intent in = new android.content.Intent(context, ble2interface.class);
			if (intent != null)
				in.putExtra("b4a_internal_intent", intent);
			context.startService(in);
		}

	}
    static ble2interface mostCurrent;
	public static BA processBA;
    private ServiceHelper _service;
    public static Class<?> getObject() {
		return ble2interface.class;
	}
	@Override
	public void onCreate() {
        mostCurrent = this;
        if (processBA == null) {
		    processBA = new BA(this, null, null, "com.iprotoxi.blue", "com.iprotoxi.blue.ble2interface");
            if (BA.isShellModeRuntimeCheck(processBA)) {
                processBA.raiseEvent2(null, true, "SHELL", false);
		    }
            try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            processBA.loadHtSubs(this.getClass());
            ServiceHelper.init();
        }
        _service = new ServiceHelper(this);
        processBA.service = this;
        processBA.setActivityPaused(false);
        if (BA.isShellModeRuntimeCheck(processBA)) {
			processBA.raiseEvent2(null, true, "CREATE", true, "com.iprotoxi.blue.ble2interface", processBA, _service);
		}
        if (!false && ServiceHelper.StarterHelper.startFromServiceCreate(processBA, false) == false) {
				
		}
		else {
            BA.LogInfo("** Service (ble2interface) Create **");
            processBA.raiseEvent(null, "service_create");
        }
        processBA.runHook("oncreate", this, null);
        if (false) {
			if (ServiceHelper.StarterHelper.waitForLayout != null)
				BA.handler.post(ServiceHelper.StarterHelper.waitForLayout);
		}
    }
		@Override
	public void onStart(android.content.Intent intent, int startId) {
		onStartCommand(intent, 0, 0);
    }
    @Override
    public int onStartCommand(final android.content.Intent intent, int flags, int startId) {
    	if (ServiceHelper.StarterHelper.onStartCommand(processBA))
			handleStart(intent);
		else {
			ServiceHelper.StarterHelper.waitForLayout = new Runnable() {
				public void run() {
                    BA.LogInfo("** Service (ble2interface) Create **");
                    processBA.raiseEvent(null, "service_create");
					handleStart(intent);
				}
			};
		}
        processBA.runHook("onstartcommand", this, new Object[] {intent, flags, startId});
		return android.app.Service.START_NOT_STICKY;
    }
    private void handleStart(android.content.Intent intent) {
    	BA.LogInfo("** Service (ble2interface) Start **");
    	java.lang.reflect.Method startEvent = processBA.htSubs.get("service_start");
    	if (startEvent != null) {
    		if (startEvent.getParameterTypes().length > 0) {
    			anywheresoftware.b4a.objects.IntentWrapper iw = new anywheresoftware.b4a.objects.IntentWrapper();
    			if (intent != null) {
    				if (intent.hasExtra("b4a_internal_intent"))
    					iw.setObject((android.content.Intent) intent.getParcelableExtra("b4a_internal_intent"));
    				else
    					iw.setObject(intent);
    			}
    			processBA.raiseEvent(null, "service_start", iw);
    		}
    		else {
    			processBA.raiseEvent(null, "service_start");
    		}
    	}
    }
	@Override
	public android.os.IBinder onBind(android.content.Intent intent) {
		return null;
	}
	@Override
	public void onDestroy() {
        BA.LogInfo("** Service (ble2interface) Destroy **");
		processBA.raiseEvent(null, "service_destroy");
        processBA.service = null;
		mostCurrent = null;
		processBA.setActivityPaused(true);
        processBA.runHook("ondestroy", this, null);
	}
public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.BleManager2 _manager = null;
public static anywheresoftware.b4a.objects.collections.List _gattserviceslist = null;
public static String _genericaccessservice = "";
public static String _genericattributeservice = "";
public static String _heartrateservice = "";
public static String _batteryservice = "";
public static String _deviceinformationservice = "";
public static String _dfuservice = "";
public static String _nrfuartservice = "";
public static String _nrfuartrxcharacteristic = "";
public static String _nrfuarttxcharacteristic = "";
public static String _batterylevelcharacteristic = "";
public static byte[] _startloggingmessage = null;
public static byte[] _stoploggingmessage = null;
public static anywheresoftware.b4a.objects.Timer _batterylevelreader = null;
public static anywheresoftware.b4a.objects.collections.List _founddevices = null;
public static anywheresoftware.b4a.objects.collections.List _maclist = null;
public static com.iprotoxi.blue.bleinterface._nameandmac _connecteddevice = null;
public static boolean _connecttolastflag = false;
public static anywheresoftware.b4a.objects.Timer _scanstoptimer = null;
public static boolean _isrealtimedata = false;
public static int _recordedpacket = 0;
public static boolean _errorflag = false;
public static int _connectstate = 0;
public static int _cstateidle = 0;
public static int _cstateconnect = 0;
public com.iprotoxi.blue.main _main = null;
public com.iprotoxi.blue.bleinterface _bleinterface = null;
public com.iprotoxi.blue.parsedata _parsedata = null;
public com.iprotoxi.blue.sensordata _sensordata = null;
public com.iprotoxi.blue.starter _starter = null;
public static String  _ble_event_connected(anywheresoftware.b4a.objects.collections.List _services) throws Exception{
String _services_ = "";
 //BA.debugLineNum = 139;BA.debugLine="Sub BLE_Event_Connected (Services As List)";
 //BA.debugLineNum = 140;BA.debugLine="Log(\"BLE_Event_Connected, \" & Services.Size & \"";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_Connected, "+BA.NumberToString(_services.getSize())+" services discovered");
 //BA.debugLineNum = 141;BA.debugLine="GATTServicesList = Services";
_gattserviceslist = _services;
 //BA.debugLineNum = 142;BA.debugLine="If ConnectState == cstateIdle Then";
if (_connectstate==_cstateidle) { 
 //BA.debugLineNum = 143;BA.debugLine="ConnectState = cstateConnect";
_connectstate = _cstateconnect;
 //BA.debugLineNum = 144;BA.debugLine="For Each services_ As String In GATTServicesLis";
final anywheresoftware.b4a.BA.IterableList group104 = _gattserviceslist;
final int groupLen104 = group104.getSize();
for (int index104 = 0;index104 < groupLen104 ;index104++){
_services_ = BA.ObjectToString(group104.Get(index104));
 //BA.debugLineNum = 145;BA.debugLine="Log(\"Manager.ReadData(services_)\" & services_)";
anywheresoftware.b4a.keywords.Common.Log("Manager.ReadData(services_)"+_services_);
 //BA.debugLineNum = 146;BA.debugLine="Manager.ReadData(services_)";
_manager.ReadData(_services_);
 }
;
 };
 //BA.debugLineNum = 149;BA.debugLine="Try";
try { //BA.debugLineNum = 150;BA.debugLine="Log(\"ConnectedDevice.MAC: \" & ConnectedDevice.M";
anywheresoftware.b4a.keywords.Common.Log("ConnectedDevice.MAC: "+_connecteddevice.MAC+", ConnectedDevice.Name: "+_connecteddevice.Name);
 //BA.debugLineNum = 151;BA.debugLine="Starter.lastConnectedMAC = ConnectedDevice.MAC";
mostCurrent._starter._lastconnectedmac = _connecteddevice.MAC;
 //BA.debugLineNum = 152;BA.debugLine="Starter.lastConnectedName = ConnectedDevice.Nam";
mostCurrent._starter._lastconnectedname = _connecteddevice.Name;
 //BA.debugLineNum = 153;BA.debugLine="Starter.APPData.PutSimple(\"MAC\", ConnectedDevic";
mostCurrent._starter._appdata._putsimple("MAC",(Object)(_connecteddevice.MAC));
 //BA.debugLineNum = 154;BA.debugLine="Starter.APPData.PutSimple(\"name\", ConnectedDevi";
mostCurrent._starter._appdata._putsimple("name",(Object)(_connecteddevice.Name));
 } 
       catch (Exception e116) {
			processBA.setLastException(e116); //BA.debugLineNum = 156;BA.debugLine="Log(\"BLE_Event_Connected Error1\" & LastExceptio";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_Connected Error1"+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 158;BA.debugLine="Try";
try { //BA.debugLineNum = 159;BA.debugLine="CallSub(Starter, \"CreateLogFile\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._starter.getObject()),"CreateLogFile");
 } 
       catch (Exception e121) {
			processBA.setLastException(e121); //BA.debugLineNum = 161;BA.debugLine="Log(\"Connected Error2\" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("Connected Error2"+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 };
 //BA.debugLineNum = 163;BA.debugLine="If IsPaused(Main) = False Then CallSubDelayed(Ma";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogHide_");};
 //BA.debugLineNum = 164;BA.debugLine="Log(\"Manager.SetNotify(nRFUartService, nRFUartTX";
anywheresoftware.b4a.keywords.Common.Log("Manager.SetNotify(nRFUartService, nRFUartTXcharacteristic, True)");
 //BA.debugLineNum = 165;BA.debugLine="SensorData.RefreshButtonStatus = \"Rescan\"";
mostCurrent._sensordata._refreshbuttonstatus = "Rescan";
 //BA.debugLineNum = 166;BA.debugLine="SensorData.BlueConnectedStatus = \"Connected\"";
mostCurrent._sensordata._blueconnectedstatus = "Connected";
 //BA.debugLineNum = 167;BA.debugLine="SensorData.ConnectedDeviceName = Starter.lastCon";
mostCurrent._sensordata._connecteddevicename = mostCurrent._starter._lastconnectedname;
 //BA.debugLineNum = 168;BA.debugLine="Delay(500, True)";
_delay((long) (500),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 169;BA.debugLine="Manager.SetNotify(nRFUartService, nRFUartTXchara";
_manager.SetNotify(_nrfuartservice,_nrfuarttxcharacteristic,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 170;BA.debugLine="Delay(500, True)";
_delay((long) (500),anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 171;BA.debugLine="Manager.SetNotify(nRFUartService, nRFUartTXchara";
_manager.SetNotify(_nrfuartservice,_nrfuarttxcharacteristic,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 174;BA.debugLine="BatteryLevelReader.Enabled = True";
_batterylevelreader.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 175;BA.debugLine="recordedPacket = 0";
_recordedpacket = (int) (0);
 //BA.debugLineNum = 176;BA.debugLine="End Sub";
return "";
}
public static String  _ble_event_dataavailable(String _serviceid,anywheresoftware.b4a.objects.collections.Map _characteristics) throws Exception{
anywheresoftware.b4a.agraham.byteconverter.ByteConverter _convert = null;
String _value = "";
String _timestamp = "";
String _datahex = "";
String _sequence = "";
String _register = "";
String _accxyz = "";
String _magxyz = "";
String _gyrxyz = "";
String _baro = "";
String _tempb = "";
String _humi = "";
 //BA.debugLineNum = 178;BA.debugLine="Sub BLE_Event_DataAvailable (ServiceId As String,";
 //BA.debugLineNum = 180;BA.debugLine="Dim convert As ByteConverter";
_convert = new anywheresoftware.b4a.agraham.byteconverter.ByteConverter();
 //BA.debugLineNum = 182;BA.debugLine="Select ServiceId";
switch (BA.switchObjectToInt(_serviceid,_genericaccessservice,_genericattributeservice,_deviceinformationservice,_heartrateservice,_batteryservice,_dfuservice,_nrfuartservice)) {
case 0:
 //BA.debugLineNum = 184;BA.debugLine="Log(\"BLE_Event_DataAvailable GenericAccessServi";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable GenericAccessService");
 break;
case 1:
 //BA.debugLineNum = 186;BA.debugLine="Log(\"BLE_Event_DataAvailable GenericAttributeSe";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable GenericAttributeService");
 break;
case 2:
 //BA.debugLineNum = 188;BA.debugLine="Log(\"BLE_Event_DataAvailable DeviceInformationS";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable DeviceInformationService");
 break;
case 3:
 //BA.debugLineNum = 190;BA.debugLine="Log(\"BLE_Event_DataAvailable HeartRateService\")";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable HeartRateService");
 break;
case 4:
 //BA.debugLineNum = 192;BA.debugLine="Log(\"BLE_Event_DataAvailable BatteryService\")";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable BatteryService");
 //BA.debugLineNum = 194;BA.debugLine="Try";
try { //BA.debugLineNum = 195;BA.debugLine="SensorData.currentBattery = \"\" & Bit.ParseInt(";
mostCurrent._sensordata._currentbattery = ""+BA.NumberToString(anywheresoftware.b4a.keywords.Common.Bit.ParseInt(_convert.HexFromBytes((byte[])(_characteristics.Get((Object)(_batterylevelcharacteristic)))),(int) (16)));
 } 
       catch (Exception e151) {
			processBA.setLastException(e151); //BA.debugLineNum = 197;BA.debugLine="Log(\"BLE_Event_DataAvailable BatteryService: \"";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable BatteryService: "+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 //BA.debugLineNum = 198;BA.debugLine="errorFlag = True";
_errorflag = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 199;BA.debugLine="Log (\"errorFlag = \" & errorFlag )";
anywheresoftware.b4a.keywords.Common.Log("errorFlag = "+BA.ObjectToString(_errorflag));
 };
 break;
case 5:
 //BA.debugLineNum = 202;BA.debugLine="Log(\"BLE_Event_DataAvailable DFUService\")";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DataAvailable DFUService");
 break;
case 6:
 //BA.debugLineNum = 206;BA.debugLine="Dim value As String";
_value = "";
 //BA.debugLineNum = 207;BA.debugLine="Dim timestamp As String = DateTime.Date(DateTim";
_timestamp = anywheresoftware.b4a.keywords.Common.DateTime.Date(anywheresoftware.b4a.keywords.Common.DateTime.getNow());
 //BA.debugLineNum = 208;BA.debugLine="Dim dataHex As String = convert.HexFromBytes( C";
_datahex = _convert.HexFromBytes((byte[])(_characteristics.Get((Object)(_nrfuarttxcharacteristic))));
 //BA.debugLineNum = 210;BA.debugLine="If dataHex <> \"\" Then";
if ((_datahex).equals("") == false) { 
 //BA.debugLineNum = 211;BA.debugLine="Dim sequence As String = dataHex.SubString2(0,";
_sequence = _datahex.substring((int) (0),(int) (2));
 //BA.debugLineNum = 212;BA.debugLine="If sequence.IndexOf(\"8\") = 0 Or sequence.Index";
if (_sequence.indexOf("8")==0 || _sequence.indexOf("9")==0) { 
 //BA.debugLineNum = 214;BA.debugLine="isRealtimeData = False";
_isrealtimedata = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 215;BA.debugLine="recordedPacket = recordedPacket + 1";
_recordedpacket = (int) (_recordedpacket+1);
 }else {
 //BA.debugLineNum = 218;BA.debugLine="isRealtimeData = True";
_isrealtimedata = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 219;BA.debugLine="recordedPacket = 0";
_recordedpacket = (int) (0);
 };
 //BA.debugLineNum = 222;BA.debugLine="Dim register As String = dataHex.SubString2(2,";
_register = _datahex.substring((int) (2),(int) (4));
 //BA.debugLineNum = 223;BA.debugLine="Select register";
switch (BA.switchObjectToInt(_register,"F1","F2")) {
case 0:
 //BA.debugLineNum = 226;BA.debugLine="Dim accXYZ As String = ParseData.Acceleration";
_accxyz = mostCurrent._parsedata._accelerationtocsv(processBA,_datahex.substring((int) (4),(int) (13)));
 //BA.debugLineNum = 227;BA.debugLine="SensorData.currentAcceleration = ParseData.Pa";
mostCurrent._sensordata._currentacceleration = mostCurrent._parsedata._parseresulttostring2(processBA,_accxyz);
 //BA.debugLineNum = 229;BA.debugLine="Dim magXYZ As String = ParseData.MagnetismToC";
_magxyz = mostCurrent._parsedata._magnetismtocsv(processBA,_datahex.substring((int) (14),(int) (23)));
 //BA.debugLineNum = 230;BA.debugLine="SensorData.currentMagnetism = ParseData.Parse";
mostCurrent._sensordata._currentmagnetism = mostCurrent._parsedata._parseresulttostring2(processBA,_magxyz);
 //BA.debugLineNum = 232;BA.debugLine="Dim gyrXYZ As String = ParseData.RotationToCS";
_gyrxyz = mostCurrent._parsedata._rotationtocsv(processBA,_datahex.substring((int) (24),(int) (33)));
 //BA.debugLineNum = 233;BA.debugLine="SensorData.currentRotation = ParseData.ParseR";
mostCurrent._sensordata._currentrotation = mostCurrent._parsedata._parseresulttostring2(processBA,_gyrxyz);
 //BA.debugLineNum = 235;BA.debugLine="If (Starter.logFlag) Then";
if ((mostCurrent._starter._logflag)) { 
 //BA.debugLineNum = 236;BA.debugLine="value = timestamp &\";\"& sequence &\";\"& regis";
_value = _timestamp+";"+_sequence+";"+_register+";"+_accxyz+";"+_magxyz+";"+_gyrxyz;
 //BA.debugLineNum = 237;BA.debugLine="If (Starter.CSVstore) Then CallSubDelayed2(S";
if ((mostCurrent._starter._csvstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"WriteDataToFile",(Object)(_value));};
 //BA.debugLineNum = 238;BA.debugLine="If (Starter.SQLstore) Then CallSubDelayed2(S";
if ((mostCurrent._starter._sqlstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"StoreData",(Object)(_value));};
 };
 break;
case 1:
 //BA.debugLineNum = 243;BA.debugLine="Dim baro As String = ParseData.parseHexToAirP";
_baro = BA.NumberToString(mostCurrent._parsedata._parsehextoairpressure2(processBA,_datahex.substring((int) (4),(int) (8))));
 //BA.debugLineNum = 244;BA.debugLine="SensorData.currentAirPressure = ParseData.Par";
mostCurrent._sensordata._currentairpressure = mostCurrent._parsedata._parseresulttostring(processBA,(float)(Double.parseDouble(_baro)));
 //BA.debugLineNum = 246;BA.debugLine="Dim tempB As String = ParseData.ParseHexToFlo";
_tempb = BA.NumberToString(mostCurrent._parsedata._parsehextofloat(processBA,_datahex.substring((int) (8),(int) (12))));
 //BA.debugLineNum = 247;BA.debugLine="SensorData.currentAirTemp = ParseData.ParseRe";
mostCurrent._sensordata._currentairtemp = mostCurrent._parsedata._parseresulttostring(processBA,(float)(Double.parseDouble(_tempb)));
 //BA.debugLineNum = 248;BA.debugLine="If dataHex.Length > 19 Then 'BTL_3X1 type of";
if (_datahex.length()>19) { 
 //BA.debugLineNum = 250;BA.debugLine="Dim humi As String = ParseData.ParseHexToFlo";
_humi = BA.NumberToString(mostCurrent._parsedata._parsehextofloat(processBA,_datahex.substring((int) (12),(int) (16))));
 //BA.debugLineNum = 251;BA.debugLine="SensorData.currentAirHumidity = ParseData.Pa";
mostCurrent._sensordata._currentairhumidity = mostCurrent._parsedata._parseresulttostring(processBA,(float)(Double.parseDouble(_humi)));
 //BA.debugLineNum = 255;BA.debugLine="value = timestamp &\";\"& sequence &\";\"& regis";
_value = _timestamp+";"+_sequence+";"+_register+";"+_baro+";"+_tempb+";"+_humi;
 }else {
 //BA.debugLineNum = 257;BA.debugLine="value = timestamp &\";\"& sequence &\";\"& regis";
_value = _timestamp+";"+_sequence+";"+_register+";"+_baro+";"+_tempb;
 };
 //BA.debugLineNum = 260;BA.debugLine="If (Starter.logFlag) Then";
if ((mostCurrent._starter._logflag)) { 
 //BA.debugLineNum = 261;BA.debugLine="If (Starter.CSVstore) Then CallSubDelayed2(S";
if ((mostCurrent._starter._csvstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"WriteDataToFile",(Object)(_value));};
 //BA.debugLineNum = 262;BA.debugLine="If (Starter.SQLstore) Then CallSubDelayed2(S";
if ((mostCurrent._starter._sqlstore)) { 
anywheresoftware.b4a.keywords.Common.CallSubDelayed2(processBA,(Object)(mostCurrent._starter.getObject()),"StoreData",(Object)(_value));};
 };
 break;
}
;
 }else {
 //BA.debugLineNum = 267;BA.debugLine="Log(\"dataHex: null\")";
anywheresoftware.b4a.keywords.Common.Log("dataHex: null");
 //BA.debugLineNum = 268;BA.debugLine="If (errorFlag) Then";
if ((_errorflag)) { 
 //BA.debugLineNum = 269;BA.debugLine="DisconnectBLE";
_disconnectble();
 //BA.debugLineNum = 270;BA.debugLine="ToastMessageShow(\"Error in Battery Service af";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Error in Battery Service after connection",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 271;BA.debugLine="errorFlag = False";
_errorflag = anywheresoftware.b4a.keywords.Common.False;
 };
 };
 break;
default:
 //BA.debugLineNum = 275;BA.debugLine="Log(\"Something else\")";
anywheresoftware.b4a.keywords.Common.Log("Something else");
 break;
}
;
 //BA.debugLineNum = 277;BA.debugLine="End Sub";
return "";
}
public static String  _ble_event_devicefound(String _name,String _deviceid,anywheresoftware.b4a.objects.collections.Map _advertisingdata,double _rssi) throws Exception{
com.iprotoxi.blue.bleinterface._nameandmac _nameandmac = null;
 //BA.debugLineNum = 89;BA.debugLine="Sub BLE_Event_DeviceFound (Name As String, Device";
 //BA.debugLineNum = 90;BA.debugLine="Log(\"BLE_Event_DeviceFound, Name: \" & Name & \",";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_DeviceFound, Name: "+_name+", DeviceId: "+_deviceid+", AdvertisingData: "+BA.ObjectToString(_advertisingdata)+" RSSI: "+BA.NumberToString(_rssi));
 //BA.debugLineNum = 97;BA.debugLine="If IsPaused(Main) = False Then CallSub(Main, \"Pr";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogHide_");};
 //BA.debugLineNum = 98;BA.debugLine="If DeviceId <> Null Then";
if (_deviceid!= null) { 
 //BA.debugLineNum = 99;BA.debugLine="If Name.Contains(\"BTL\") Then";
if (_name.contains("BTL")) { 
 //BA.debugLineNum = 100;BA.debugLine="Dim nameAndMac As NameAndMAC";
_nameandmac = new com.iprotoxi.blue.bleinterface._nameandmac();
 //BA.debugLineNum = 101;BA.debugLine="nameAndMac.Initialize";
_nameandmac.Initialize();
 //BA.debugLineNum = 102;BA.debugLine="nameAndMac.Name = Name";
_nameandmac.Name = _name;
 //BA.debugLineNum = 103;BA.debugLine="nameAndMac.Mac = DeviceId";
_nameandmac.MAC = _deviceid;
 //BA.debugLineNum = 104;BA.debugLine="MACList.Add(DeviceId)";
_maclist.Add((Object)(_deviceid));
 //BA.debugLineNum = 105;BA.debugLine="FoundDevices.Add(nameAndMac)";
_founddevices.Add((Object)(_nameandmac));
 //BA.debugLineNum = 106;BA.debugLine="Log(\"Found devices: \" & FoundDevices.Size)";
anywheresoftware.b4a.keywords.Common.Log("Found devices: "+BA.NumberToString(_founddevices.getSize()));
 //BA.debugLineNum = 107;BA.debugLine="If IsPaused(Main) = False Then CallSub2(Main,";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew2(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogShow_",(Object)("~ device(s) found...".replace("~",BA.NumberToString(_founddevices.getSize()))));};
 //BA.debugLineNum = 108;BA.debugLine="Log(\"ConnectToLastFlag: \" & ConnectToLastFlag)";
anywheresoftware.b4a.keywords.Common.Log("ConnectToLastFlag: "+BA.ObjectToString(_connecttolastflag));
 //BA.debugLineNum = 109;BA.debugLine="If (ConnectToLastFlag) Then";
if ((_connecttolastflag)) { 
 //BA.debugLineNum = 110;BA.debugLine="If DeviceId = Starter.lastConnectedMAC Then";
if ((_deviceid).equals(mostCurrent._starter._lastconnectedmac)) { 
 //BA.debugLineNum = 111;BA.debugLine="Manager.Connect(DeviceId)";
_manager.Connect(_deviceid);
 //BA.debugLineNum = 112;BA.debugLine="Manager.StopScan";
_manager.StopScan();
 //BA.debugLineNum = 113;BA.debugLine="ScanStopTimer.Enabled = False";
_scanstoptimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 114;BA.debugLine="ConnectedDevice.MAC = DeviceId";
_connecteddevice.MAC = _deviceid;
 //BA.debugLineNum = 115;BA.debugLine="ConnectedDevice.Name = Name";
_connecteddevice.Name = _name;
 };
 };
 };
 };
 //BA.debugLineNum = 120;BA.debugLine="End Sub";
return "";
}
public static String  _ble_event_disconnected() throws Exception{
 //BA.debugLineNum = 122;BA.debugLine="Sub BLE_Event_Disconnected";
 //BA.debugLineNum = 123;BA.debugLine="Log(\"BLE_Event_Disconnected\")";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_Disconnected");
 //BA.debugLineNum = 124;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 //BA.debugLineNum = 125;BA.debugLine="BatteryLevelReader.Enabled = False";
_batterylevelreader.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 126;BA.debugLine="SensorData.ConnectedDeviceName = \"Aistin Blue\"";
mostCurrent._sensordata._connecteddevicename = "Aistin Blue";
 //BA.debugLineNum = 127;BA.debugLine="SensorData.BlueConnectedStatus = \"Disconnected\"";
mostCurrent._sensordata._blueconnectedstatus = "Disconnected";
 //BA.debugLineNum = 128;BA.debugLine="SensorData.RefreshButtonStatus = \"Scan\"";
mostCurrent._sensordata._refreshbuttonstatus = "Scan";
 //BA.debugLineNum = 129;BA.debugLine="SensorData.currentAirTemp = \"---\"";
mostCurrent._sensordata._currentairtemp = "---";
 //BA.debugLineNum = 130;BA.debugLine="SensorData.currentAirHumidity = \"---\"";
mostCurrent._sensordata._currentairhumidity = "---";
 //BA.debugLineNum = 131;BA.debugLine="SensorData.currentAirPressure = \"---\"";
mostCurrent._sensordata._currentairpressure = "---";
 //BA.debugLineNum = 132;BA.debugLine="SensorData.currentAcceleration = \"---\"";
mostCurrent._sensordata._currentacceleration = "---";
 //BA.debugLineNum = 133;BA.debugLine="SensorData.currentMagnetism = \"---\"";
mostCurrent._sensordata._currentmagnetism = "---";
 //BA.debugLineNum = 134;BA.debugLine="SensorData.currentRotation = \"---\"";
mostCurrent._sensordata._currentrotation = "---";
 //BA.debugLineNum = 135;BA.debugLine="SensorData.currentBattery = \"---\"";
mostCurrent._sensordata._currentbattery = "---";
 //BA.debugLineNum = 136;BA.debugLine="ToastMessageShow(\"Disconnected\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Disconnected",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 137;BA.debugLine="End Sub";
return "";
}
public static String  _ble_event_statechanged(int _state) throws Exception{
 //BA.debugLineNum = 78;BA.debugLine="Sub BLE_Event_StateChanged (State As Int)";
 //BA.debugLineNum = 79;BA.debugLine="Select State";
switch (BA.switchObjectToInt(_state,_manager.STATE_UNSUPPORTED,_manager.STATE_POWERED_OFF,_manager.STATE_POWERED_ON)) {
case 0:
 //BA.debugLineNum = 81;BA.debugLine="Log(\"BLE_Event_StateChanged, Manager.STATE_UNS";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_StateChanged, Manager.STATE_UNSUPPORTED: "+BA.NumberToString(_state));
 break;
case 1:
 //BA.debugLineNum = 83;BA.debugLine="Log(\"BLE_Event_StateChanged, Manager.STATE_POW";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_StateChanged, Manager.STATE_POWERED_OFF: "+BA.NumberToString(_state));
 break;
case 2:
 //BA.debugLineNum = 85;BA.debugLine="Log(\"BLE_Event_StateChanged, Manager.STATE_POW";
anywheresoftware.b4a.keywords.Common.Log("BLE_Event_StateChanged, Manager.STATE_POWERED_ON: "+BA.NumberToString(_state));
 break;
}
;
 //BA.debugLineNum = 87;BA.debugLine="End Sub";
return "";
}
public static String  _delay(long _nmillisecond,boolean _enableevents) throws Exception{
long _nbegintime = 0L;
long _nendtime = 0L;
 //BA.debugLineNum = 336;BA.debugLine="Sub Delay (nMilliSecond As Long, EnableEvents As B";
 //BA.debugLineNum = 337;BA.debugLine="Dim nBeginTime, nEndTime As Long";
_nbegintime = 0L;
_nendtime = 0L;
 //BA.debugLineNum = 338;BA.debugLine="nEndTime = DateTime.Now + nMilliSecond";
_nendtime = (long) (anywheresoftware.b4a.keywords.Common.DateTime.getNow()+_nmillisecond);
 //BA.debugLineNum = 339;BA.debugLine="nBeginTime = DateTime.Now";
_nbegintime = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 340;BA.debugLine="Do While nBeginTime < nEndTime";
while (_nbegintime<_nendtime) {
 //BA.debugLineNum = 341;BA.debugLine="nBeginTime = DateTime.Now";
_nbegintime = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 342;BA.debugLine="If nEndTime < nBeginTime Then Return";
if (_nendtime<_nbegintime) { 
if (true) return "";};
 //BA.debugLineNum = 343;BA.debugLine="If (EnableEvents) Then DoEvents";
if ((_enableevents)) { 
anywheresoftware.b4a.keywords.Common.DoEvents();};
 }
;
 //BA.debugLineNum = 345;BA.debugLine="End Sub";
return "";
}
public static String  _disconnectble() throws Exception{
 //BA.debugLineNum = 299;BA.debugLine="Sub DisconnectBLE";
 //BA.debugLineNum = 300;BA.debugLine="Try";
try { //BA.debugLineNum = 301;BA.debugLine="Manager.Disconnect";
_manager.Disconnect();
 //BA.debugLineNum = 302;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 } 
       catch (Exception e230) {
			processBA.setLastException(e230); //BA.debugLineNum = 304;BA.debugLine="Log(\"DisconnectBLE Error: \" & LastException)";
anywheresoftware.b4a.keywords.Common.Log("DisconnectBLE Error: "+BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(processBA)));
 //BA.debugLineNum = 305;BA.debugLine="StopService(\"\") 'stop self";
anywheresoftware.b4a.keywords.Common.StopService(processBA,(Object)(""));
 };
 //BA.debugLineNum = 307;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 7;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 10;BA.debugLine="Dim Manager As BleManager2";
_manager = new anywheresoftware.b4a.objects.BleManager2();
 //BA.debugLineNum = 12;BA.debugLine="Dim GATTServicesList As List";
_gattserviceslist = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 14;BA.debugLine="Private GenericAccessService As String 		= \"00001";
_genericaccessservice = "00001800-0000-1000-8000-00805f9b34fb";
 //BA.debugLineNum = 15;BA.debugLine="Private GenericAttributeService As String 	= \"000";
_genericattributeservice = "00001801-0000-1000-8000-00805f9b34fb";
 //BA.debugLineNum = 16;BA.debugLine="Private HeartRateService As String 			= \"0000180d";
_heartrateservice = "0000180d-0000-1000-8000-00805f9b34fb";
 //BA.debugLineNum = 17;BA.debugLine="Private BatteryService As String 			= \"0000180f-0";
_batteryservice = "0000180f-0000-1000-8000-00805f9b34fb";
 //BA.debugLineNum = 18;BA.debugLine="Private DeviceInformationService As String 	= \"00";
_deviceinformationservice = "0000180a-0000-1000-8000-00805f9b34fb";
 //BA.debugLineNum = 19;BA.debugLine="Private DFUService As String 				= \"00001530-1212";
_dfuservice = "00001530-1212-efde-1523-785feabcd123";
 //BA.debugLineNum = 20;BA.debugLine="Private nRFUartService As String 			= \"6e400001-b";
_nrfuartservice = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
 //BA.debugLineNum = 22;BA.debugLine="Private nRFUartRXcharacteristic As String 	= \"6e4";
_nrfuartrxcharacteristic = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
 //BA.debugLineNum = 23;BA.debugLine="Private nRFUartTXcharacteristic As String 	= \"6e4";
_nrfuarttxcharacteristic = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
 //BA.debugLineNum = 25;BA.debugLine="Private BatteryLevelCharacteristic As String = \"0";
_batterylevelcharacteristic = "00002a19-0000-1000-8000-00805f9b34fb";
 //BA.debugLineNum = 27;BA.debugLine="Private startLoggingMessage() As Byte = Array As";
_startloggingmessage = new byte[]{(byte) (0x04),(byte) (0x01),(byte) (0x01),(byte) (0x86),(byte) (0x07),(byte) (0x03)};
 //BA.debugLineNum = 28;BA.debugLine="Private stopLoggingMessage()  As Byte = Array As";
_stoploggingmessage = new byte[]{(byte) (0x04),(byte) (0x01),(byte) (0x01),(byte) (0x86),(byte) (0x07),(byte) (0x00)};
 //BA.debugLineNum = 30;BA.debugLine="Dim BatteryLevelReader As Timer";
_batterylevelreader = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 33;BA.debugLine="Dim FoundDevices As List";
_founddevices = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 34;BA.debugLine="Dim MACList As List";
_maclist = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 35;BA.debugLine="Dim ConnectedDevice As NameAndMAC";
_connecteddevice = new com.iprotoxi.blue.bleinterface._nameandmac();
 //BA.debugLineNum = 36;BA.debugLine="Dim ConnectToLastFlag As Boolean";
_connecttolastflag = false;
 //BA.debugLineNum = 38;BA.debugLine="Private ScanStopTimer As Timer";
_scanstoptimer = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 40;BA.debugLine="Dim isRealtimeData As Boolean";
_isrealtimedata = false;
 //BA.debugLineNum = 41;BA.debugLine="Dim recordedPacket As Int";
_recordedpacket = 0;
 //BA.debugLineNum = 43;BA.debugLine="Dim errorFlag As Boolean";
_errorflag = false;
 //BA.debugLineNum = 45;BA.debugLine="Dim ConnectState As Int";
_connectstate = 0;
 //BA.debugLineNum = 46;BA.debugLine="Dim cstateIdle As Int = 0";
_cstateidle = (int) (0);
 //BA.debugLineNum = 48;BA.debugLine="Dim cstateConnect As Int = 2";
_cstateconnect = (int) (2);
 //BA.debugLineNum = 53;BA.debugLine="End Sub";
return "";
}
public static String  _readbatterylevel_tick() throws Exception{
 //BA.debugLineNum = 331;BA.debugLine="Sub ReadBatteryLevel_Tick";
 //BA.debugLineNum = 333;BA.debugLine="Manager.ReadData2(BatteryService, BatteryLevelCha";
_manager.ReadData2(_batteryservice,_batterylevelcharacteristic);
 //BA.debugLineNum = 334;BA.debugLine="End Sub";
return "";
}
public static String  _scanfordevices() throws Exception{
 //BA.debugLineNum = 280;BA.debugLine="Sub ScanForDevices 'ConnectToDevice";
 //BA.debugLineNum = 281;BA.debugLine="ToastMessageShow(\"Searching for devices...\", Fals";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Searching for devices...",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 282;BA.debugLine="ScanStopTimer.Enabled = True";
_scanstoptimer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 283;BA.debugLine="FoundDevices.Clear";
_founddevices.Clear();
 //BA.debugLineNum = 284;BA.debugLine="Manager.Scan2(Null, False)";
_manager.Scan2((anywheresoftware.b4a.objects.collections.List) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4a.objects.collections.List(), (java.util.List)(anywheresoftware.b4a.keywords.Common.Null)),anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 285;BA.debugLine="End Sub";
return "";
}
public static String  _scanstoptimer_tick() throws Exception{
anywheresoftware.b4a.objects.collections.List _devices = null;
int _i = 0;
com.iprotoxi.blue.bleinterface._nameandmac _nameandmac = null;
 //BA.debugLineNum = 309;BA.debugLine="Sub ScanStopTimer_Tick";
 //BA.debugLineNum = 310;BA.debugLine="Log(\"ScanStopTimer_Tick\")";
anywheresoftware.b4a.keywords.Common.Log("ScanStopTimer_Tick");
 //BA.debugLineNum = 311;BA.debugLine="ScanStopTimer.Enabled = False";
_scanstoptimer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 312;BA.debugLine="Manager.StopScan";
_manager.StopScan();
 //BA.debugLineNum = 313;BA.debugLine="If IsPaused(Main) = False Then CallSub(Main, \"Pro";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._main.getObject()),"ProgressDialogHide_");};
 //BA.debugLineNum = 315;BA.debugLine="If FoundDevices.Size == 0 Then";
if (_founddevices.getSize()==0) { 
 //BA.debugLineNum = 316;BA.debugLine="ToastMessageShow(\"No new devices found\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("No new devices found",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 318;BA.debugLine="Dim devices As List";
_devices = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 319;BA.debugLine="devices.Initialize";
_devices.Initialize();
 //BA.debugLineNum = 320;BA.debugLine="For i = 0 To FoundDevices.Size - 1";
{
final int step244 = 1;
final int limit244 = (int) (_founddevices.getSize()-1);
for (_i = (int) (0); (step244 > 0 && _i <= limit244) || (step244 < 0 && _i >= limit244); _i = ((int)(0 + _i + step244))) {
 //BA.debugLineNum = 321;BA.debugLine="Dim nameAndMac As NameAndMAC";
_nameandmac = new com.iprotoxi.blue.bleinterface._nameandmac();
 //BA.debugLineNum = 322;BA.debugLine="nameAndMac = FoundDevices.Get(i)";
_nameandmac = (com.iprotoxi.blue.bleinterface._nameandmac)(_founddevices.Get(_i));
 //BA.debugLineNum = 323;BA.debugLine="devices.Add(nameAndMac.Name & \" (\" & nameAndMac";
_devices.Add((Object)(_nameandmac.Name+" ("+_nameandmac.MAC+")"));
 }
};
 //BA.debugLineNum = 325;BA.debugLine="If IsPaused(Main) = False Then CallSub2(Main, \"S";
if (anywheresoftware.b4a.keywords.Common.IsPaused(processBA,(Object)(mostCurrent._main.getObject()))==anywheresoftware.b4a.keywords.Common.False) { 
anywheresoftware.b4a.keywords.Common.CallSubNew2(processBA,(Object)(mostCurrent._main.getObject()),"SelectDeviceAndConnect",(Object)(_devices));};
 //BA.debugLineNum = 326;BA.debugLine="FoundDevices.Clear";
_founddevices.Clear();
 //BA.debugLineNum = 327;BA.debugLine="devices.Clear";
_devices.Clear();
 };
 //BA.debugLineNum = 329;BA.debugLine="End Sub";
return "";
}
public static String  _service_create() throws Exception{
 //BA.debugLineNum = 55;BA.debugLine="Sub Service_Create";
 //BA.debugLineNum = 56;BA.debugLine="ScanStopTimer.Initialize(\"ScanStopTimer\", 4*1000)";
_scanstoptimer.Initialize(processBA,"ScanStopTimer",(long) (4*1000));
 //BA.debugLineNum = 57;BA.debugLine="MACList.Initialize";
_maclist.Initialize();
 //BA.debugLineNum = 58;BA.debugLine="FoundDevices.Initialize";
_founddevices.Initialize();
 //BA.debugLineNum = 59;BA.debugLine="ConnectedDevice.Initialize";
_connecteddevice.Initialize();
 //BA.debugLineNum = 60;BA.debugLine="GATTServicesList.Initialize";
_gattserviceslist.Initialize();
 //BA.debugLineNum = 61;BA.debugLine="Manager.Initialize(\"BLE_Event\")";
_manager.Initialize(processBA,"BLE_Event");
 //BA.debugLineNum = 62;BA.debugLine="BatteryLevelReader.Initialize(\"ReadBatteryLevel\",";
_batterylevelreader.Initialize(processBA,"ReadBatteryLevel",(long) (0.6*60*1000));
 //BA.debugLineNum = 63;BA.debugLine="ConnectState = cstateIdle";
_connectstate = _cstateidle;
 //BA.debugLineNum = 64;BA.debugLine="End Sub";
return "";
}
public static String  _service_destroy() throws Exception{
 //BA.debugLineNum = 72;BA.debugLine="Sub Service_Destroy";
 //BA.debugLineNum = 73;BA.debugLine="CallSub(Starter, \"CloseTextwriter\")";
anywheresoftware.b4a.keywords.Common.CallSubNew(processBA,(Object)(mostCurrent._starter.getObject()),"CloseTextwriter");
 //BA.debugLineNum = 74;BA.debugLine="StopService(\"\") 'stop self";
anywheresoftware.b4a.keywords.Common.StopService(processBA,(Object)(""));
 //BA.debugLineNum = 75;BA.debugLine="End Sub";
return "";
}
public static String  _service_start(anywheresoftware.b4a.objects.IntentWrapper _startingintent) throws Exception{
 //BA.debugLineNum = 66;BA.debugLine="Sub Service_Start (StartingIntent As Intent)";
 //BA.debugLineNum = 67;BA.debugLine="ConnectToLastFlag = True";
_connecttolastflag = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 68;BA.debugLine="isRealtimeData = True";
_isrealtimedata = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 69;BA.debugLine="errorFlag = False";
_errorflag = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 70;BA.debugLine="End Sub";
return "";
}
public static String  _writebytesstartlogging() throws Exception{
 //BA.debugLineNum = 287;BA.debugLine="Sub WriteBytesStartLogging()";
 //BA.debugLineNum = 290;BA.debugLine="Manager.WriteData(nRFUartService, nRFUartRXcharac";
_manager.WriteData(_nrfuartservice,_nrfuartrxcharacteristic,_startloggingmessage);
 //BA.debugLineNum = 291;BA.debugLine="End Sub";
return "";
}
public static String  _writebytesstoplogging() throws Exception{
 //BA.debugLineNum = 293;BA.debugLine="Sub WriteBytesStopLogging()";
 //BA.debugLineNum = 296;BA.debugLine="Manager.WriteData(nRFUartService, nRFUartRXcharac";
_manager.WriteData(_nrfuartservice,_nrfuartrxcharacteristic,_stoploggingmessage);
 //BA.debugLineNum = 297;BA.debugLine="End Sub";
return "";
}
}
